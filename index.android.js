import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';

import App from './App/App';

export default class TradingAppFrontEnd extends Component {
  render() {
    return (
      <View style={styles.container}>
        <App />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#353535'
  }
});

AppRegistry.registerComponent('TradingAppFrontEnd', () => TradingAppFrontEnd);
