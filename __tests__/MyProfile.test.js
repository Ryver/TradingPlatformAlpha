import 'react-native';
import React from 'react';
import MyProfile from '../App/Components/MyProfile/MyProfile';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

describe('Component: MyProfile', () => {

  const user = {
    email: 'john@doe.com',
    firstName: 'John',
    lastName: 'Doe',
    myFxBook: null,
    ownWebsite: null,
    expirience: 5
  }

  it('renders correctly', () => {
    const tree = renderer.create(
     <MyProfile user={ user }/>
    )
  });
})