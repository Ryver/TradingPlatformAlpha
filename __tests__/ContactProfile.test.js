import 'react-native';
import React from 'react';
import ContactProfile from '../App/Components/ContactProfile/ContactProfile';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

describe('Component: MyContacts', () => {
  it('renders correctly', () => {
    const friend = {
      email: 'test@test.com',
      firstName: 'John',
      lastName: 'Doe',
      myFxBook: null,
      ownWebsite: null,
      tradingExp: null
    }
    const tree = renderer.create(
     <ContactProfile friend={ friend } />
    )
  });
})