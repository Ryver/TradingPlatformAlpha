import 'react-native';
import React from 'react';
import BOStrike from '../App/Components/Calculators/BOStrike';
import FxPositionSize from '../App/Components/Calculators/FxPositionSize';
import FxProfit from '../App/Components/Calculators/FxProfit';
import FxSL from '../App/Components/Calculators/FxSL';
import StocksProfit from '../App/Components/Calculators/StocksProfit';


// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

describe('Component: BOStrike calculator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
     <BOStrike />
    )
  });
});

describe('Component: FxPositionSize calculator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
     <FxPositionSize />
    )
  });
});

describe('Component: FxProfit calculator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
     <FxProfit />
    )
  });
});

describe('Component: FxSL calculator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
     <FxSL />
    )
  });
});

describe('Component: StocksProfit calculator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
     <StocksProfit />
    )
  });
});