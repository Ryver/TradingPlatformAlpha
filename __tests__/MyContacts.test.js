import 'react-native';
import React from 'react';
import MyContacts from '../App/Components/MyContacts/MyContacts';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

describe('Component: MyContacts', () => {

  const user = {
    username: 'test',
    contacts: []
  }

  it('renders correctly', () => {
    const tree = renderer.create(
     <MyContacts user={ user } />
    )
  });
})