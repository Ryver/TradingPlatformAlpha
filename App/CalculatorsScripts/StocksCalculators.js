export async function profitCalc(startVal, closeVal, amount) {
  const result = (closeVal - startVal) * amount;

  return result.toFixed(2)
};