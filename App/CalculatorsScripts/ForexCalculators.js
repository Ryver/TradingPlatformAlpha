function getCurrenciesCourses(pairCurrency, accCurrency) {
  const url = `https://api.fixer.io/latest?base=${ pairCurrency }`;

  return fetch(url)
  .then((response) => response.json())
  .then((res) => res.rates[accCurrency].toFixed(2))
  .catch((err) => console.log(err))
};

export async function positionSizeCalcPercentRisk(accAmount, risk, accCurrency, slDistance, currenciesPair, levarge) {
  const positionRisk = accAmount * (risk / 100);
  const currencyCourse = accCurrency === currenciesPair ? 1 : await getCurrenciesCourses(currenciesPair, accCurrency)

  const result = positionRisk * levarge / (slDistance / 100) / currencyCourse; 
  return result.toFixed(0);
};

export async function positionSizeCalcValueRisk(risk, accCurrency, slDistance, currenciesPair, levarge) {
  const currencyCourse = accCurrency === currenciesPair ? 1 : await getCurrenciesCourses(currenciesPair, accCurrency)

  const result = risk * levarge / (slDistance / 100) / currencyCourse; 
  return result.toFixed(0);
};

export async function profitCalc(posSize, pairCurrency, accCurrency, pips, accType) {
  const currencyCourse = accCurrency === pairCurrency ? 1 : await getCurrenciesCourses(pairCurrency, accCurrency);
  const pipVal = pairCurrency === 'JPY' ? 0.01 : 0.0001;

  const result = (pipVal * (posSize * (accType === 'Standard' ? 100000 : 1000)) * currencyCourse) * pips;
  
  return result.toFixed(2);
};

export async function stopLossCalc(posSizeType, posSize, accCurrency, pairCurrency, risk) {
  const currencyCourse = accCurrency === pairCurrency ? 1 : await getCurrenciesCourses(pairCurrency, accCurrency);
  const currenciesPipVal = pairCurrency === 'JPY' ? 0.01 : 0.0001;
  
  const pipVal = currenciesPipVal * (posSize * (posSizeType === 'Standard' ? 100000 : 1000)) * currencyCourse;
  const result = risk / pipVal;

  return result.toFixed(2);
}