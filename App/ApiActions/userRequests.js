const url = 'http://192.168.1.106:3020/api/user';

export function signIn(userObj) {
  return fetch(`${ url }/signIn`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function signUp(userObj) {
  return fetch(`${ url }/signUp`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function changePassword(id, userObj) {
  return fetch(`${ url }/${ id }/changePassword`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function editProfile(id, userObj) {
  return fetch(`${ url }/${ id }/editProfile`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userObj)
  })
  .then((res) => res)
  .catch((err) => console.log(err))
};

export function getValidUsr(token) {
  return fetch(`${ url }/${ token }`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function contactsSearch(userObj) {
  return fetch(`${ url }/searchByName`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function changeOnlineStatus(id, obj) {
  return fetch(`${ url }/changeOnlineStatus/${ id }`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(obj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function addContact(id, body) {
  return fetch(`${ url }/addContact/${ id }`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function updateContacts(id) {
  return fetch(`${ url }/${ id }/contacts`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};