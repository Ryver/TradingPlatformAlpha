const url = 'http://192.168.1.106:3020/api/diary';

export function getUserDiary(id) {
  return fetch(`${ url }/getDiary/${ id }`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function createNewPost(body) {
  return fetch(`${ url }/addNote`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};