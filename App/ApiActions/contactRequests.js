const url = 'http://192.168.1.106:3020/api/contactReq';

export function getPendingContactReq(username) {
  return fetch(`${ url }/getUsrRequests/${ username }`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function newContactReq(bodyObj) {
  return fetch(`${ url }/new`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(bodyObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function acceptContactReq(bodyObj) {
  return fetch(`${ url }/acceptContactReq`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(bodyObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};

export function deleteContactReq(bodyObj) {
  return fetch(`${ url }/deleteContactReq`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(bodyObj)
  })
  .then((res) => res.json())
  .catch((err) => console.log(err))
};