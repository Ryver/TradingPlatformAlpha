import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import mainNavBar from '../../GlobalState/MainNavBar';


import Information from './Information';

class ContactProfile extends Component {

  componentWillMount() {
    mainNavBar.isNavBarVisible = true
  }

  render() {
    
    const informations = [
      {
        title: 'Email',
        val: this.props.friend.email
      },
      {
        title: 'First Name',
        val: this.props.friend.firstName
      },
      {
        title: 'Last Name',
        val: this.props.friend.lastName
      },
      {
        title: 'My FxBook',
        val: this.props.friend.myFxBook
      },
      {
        title: 'Own Website',
        val: this.props.friend.ownWebsite
      },
      {
        title: 'Trading Expirience (in years)',
        val: this.props.friend.tradingExp
      }
    ]

    const displayInformations = informations.map((info, index) =>
      <Information 
        info={ info }
        key={ index }
      />
    )

    return(
      <View style={ styles.container }>
        <View style={ styles.avatarContainer }>
          <Icon 
            name='ios-person'
            size={ 106 }
            color='#3399FF'
          />
        </View>
        <View style={ styles.infoContainer }>
          { displayInformations }
        </View>
      </View>
    )
  }
}

export default ContactProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingHorizontal: 20
  },
  avatarContainer: {
    alignItems: 'center'
  }
})