import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

class Information extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.infoName }>{ this.props.info.title }:</Text>
        <Text style={ styles.infoVal }>{ this.props.info.val }</Text>
      </View>
    )
  }
}

export default Information;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 5
  },
  infoName: {
    color: '#3399FF',
    fontFamily: 'Avenir',
    fontSize: 20
  },
  infoVal: {
    fontFamily: 'Avenir',
    color: '#FFF',
    fontSize: 20,
    marginLeft: 15
  }
})