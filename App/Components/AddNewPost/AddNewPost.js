import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { createNewPost } from '../../ApiActions/diaryRequests';
import validUserStore from '../../GlobalState/ValidUserStore';
import mainNavBar from '../../GlobalState/MainNavBar';

class AddNewPost extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.addNewPost = this.addNewPost.bind(this);

    this.state = {
      currencyPair: null,
      comment: null,
      closeDay: null,
      closeMonth: null,
      closeYear: null,
      date: null,
      balance: null
    }
  }

  componentWillUnmount() {
    mainNavBar.isNavBarVisible = true
  }

  addNewPost() {
    createNewPost({
      user: validUserStore.validUser.id,
      openDate: this.props.date,
      closeDate: this.state.date,
      balance: this.state.balance,
      comment: this.state.comment,
      pair: this.state.currencyPair
    })
    .then(() => {
      this.props.navigator.pop()
    })
    .catch((err) => console.log(err))
  }

  onChange(fieldname, val) {
    this.setState({ 
      [fieldname]: val,
      date: `${ this.state.closeYear }-${ this.state.closeMonth }-${ this.state.closeDay }`
     })
    
  }

  render() {
    return(
      <View style={ styles.container }>

        <TextInput
          style={ styles.input }
          placeholder='Currency Pair'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('currencyPair', e.nativeEvent.text.toUpperCase()) }
          value={ this.state.currencyPair }
        />

        <Text style={ styles.dateTxt }>Open Date</Text>
        <View style={ styles.dateHolder }>
          <TextInput
            style={ [
              styles.dateInput,
              { flex: 1 }
             ]}
            placeholder='DD'
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            value={ this.props.date.slice(8, 10) }
          />

          <TextInput
            style={ [
              styles.dateInput,
              { flex: 1 }
             ]}
            placeholder='MM'
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            value={ this.props.date.slice(5, 7) }
          />

        <TextInput
          style={ [
              styles.dateInput,
              { flex: 2 }
             ]}
          placeholder='YYYY'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          value={ this.props.date.slice(0, 4) }
        />
        </View>

        <Text style={ styles.dateTxt }>Close Date</Text>
        <View style={ styles.dateHolder }>
          <TextInput
            style={ [
              styles.dateInput,
              { flex: 1 }
             ]}
            placeholder='DD'
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            onChange={ (e) => this.onChange('closeDay', e.nativeEvent.text) }
            value={ this.state.closeDay }
          />

          <TextInput
            style={ [
              styles.dateInput,
              { flex: 1 }
             ]}
            placeholder='MM'
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            onChange={ (e) => this.onChange('closeMonth', e.nativeEvent.text) }
            value={ this.state.closeMonth }
          />

        <TextInput
          style={ [
              styles.dateInput,
              { flex: 2 }
             ]}
          placeholder='YYYY'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('closeYear', e.nativeEvent.text) }
          value={ this.state.closeYear }
        />
        </View>

        <TextInput
          style={ styles.input }
          placeholder='Balance'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('balance', e.nativeEvent.text) }
          value={ this.state.balance }
        />

        <TextInput
          style={ [styles.input] }
          placeholder='Comment'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('comment', e.nativeEvent.text) }
          value={ this.state.comment }
        />

        <TouchableOpacity
          style={ styles.btn }
          onPress={ ()=> this.addNewPost() }
        >
          <Text style={ styles.btnTxt }>Save Post</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default AddNewPost;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingHorizontal: 20
  },
  input: {
    fontFamily: 'Avenir',
    color: '#fff',
    borderWidth: 0.5,
    borderColor: '#FFF',
    backgroundColor: '#444444',
    height: 40,
    paddingLeft: 10,
    marginBottom: 20
  },
  dateHolder: {
    flexDirection: 'row',
    marginBottom: 20
  },
  dateInput: {
    borderWidth: 0.5,
    borderColor: '#FFF',
    backgroundColor: '#444444',
    height: 40,
    paddingHorizontal: 10,
    marginRight: 10,
    color: '#FFF'
  },
  dateTxt: {
    color: '#FFF',
    fontFamily: 'Avenir',
    marginBottom: 10,
    fontSize: 18,
    textAlign: 'center'
  },
  btn: {
    height: 35,
    backgroundColor: '#282828',
    borderWidth: 0.5,
    borderColor: '#3399FF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTxt: {
    fontFamily: 'Avenir',
    color: '#FFF',
    fontSize: 16
  }
})