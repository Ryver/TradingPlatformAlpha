import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
  Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import globalNavStore from '../../GlobalState/GlobalNavStore';
import { observer } from 'mobx-react';

import Section from './Section';

class Navigation extends Component {

  constructor(props) {
    super(props);

    this.state = {
      componentPosition: new Animated.Value(Dimensions.get('window').width)
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.isMenuOpen !== this.props.isMenuOpen) {
      Animated.timing(
        this.state.componentPosition, {
          toValue: nextProps.isMenuOpen ? 0 : Dimensions.get('window').width,
          duration: 300
        }
      ).start((e) => console.log(e));
    }
  }

  render() {
    console.log(this.state.componentPosition)
    const sections = [
      {
        name: 'Calculators',
        symbol: 'calculator',
        subSections: [
          'Forex Calculators',
          'BO Calculators',
          'Stocks Calculators'
        ]
      },
      {
        name: 'Charts',
        symbol: 'line-chart',
        subSections: [
          'My Charts',
          'Forex Market',
          'Stocks Market'
        ]
      },
      {
        name: 'Diaries',
        symbol: 'book',
        subSections: [
          'Forex Diary',
          'Binary Options Diary'
        ]
      },
      {
        name: 'Profile',
        symbol: 'user',
        subSections: [
          'My Profile',
          'Change Password'
        ]
      },
      {
        name: 'Contacts',
        symbol: 'address-card',
        subSections: [
          'My Contacts'
        ]
      },
      {
        name: 'Preferences',
        symbol: 'cogs',
        subSections: [
          'Application Settings',
          'Sign Out'
        ]
      }
    ]

    const mappedSections = sections.map((section, index) => 
      <Section
        section={ section }
        key={ index }
        nav={ this.props.navigator }
      />
    )

    return(
      <Animated.View style={[
        styles.container, 
        { right: this.state.componentPosition }
        ]}>
        <View style={ styles.topBar }>
          <TouchableOpacity onPress={ () => { globalNavStore.isNavOpen = false } }>
            <Icon 
              name='ios-close'
              size={ 75 }
              color='#CC6600'
            />
          </TouchableOpacity>
          <Image 
            source={ require('../../Styles/Images/TradingAppLogo.png') }
            style={ styles.logo }
          />
          <View style={{ flex: 1 }} />
        </View>
        <View style={ styles.sections }>
          { mappedSections }
        </View>
      </Animated.View>
    );
  }
}

export default observer(Navigation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 20,
    paddingTop: 20,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.95)'
  },
  topBar: {
    paddingLeft: 20,
    flexDirection: 'row',
    marginBottom: 10
  },
  sections: {
    flex: 10,
  },
  closeIcon: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#CC6600',
    flex: 1
  },
  logo: {
    flex: 4,
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
    marginLeft: 10
  }
})