import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import globalNavStore from '../../GlobalState/GlobalNavStore';
import { observer } from 'mobx-react';
import validUserStore from '../../GlobalState/ValidUserStore';
import { changeOnlineStatus } from '../../ApiActions/userRequests';
import { getUserDiary } from '../../ApiActions/diaryRequests';


class Section extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isSubMenuOpen: false
    }
  }

  render() {

    const subSections = this.props.section.subSections.map((subSection, index) =>
       <TouchableOpacity 
        onPress={ () => {
          this.setState({ isSubMenuOpen: false })
          globalNavStore.isNavOpen = false;
          if(this.props.section.name === 'Calculators') {
            this.props.nav.replace({ 
              name: 'calculators',
              passProps: {
                sectionName: subSection,
                sectionIndex: index
              } 
            })
          } else if(this.props.section.name === 'Preferences') {
            if(subSection === 'Application Settings') {
              this.props.nav.replace({
                name: 'settingsScreen'
              })
            } else {
              AsyncStorage.setItem('TradingAppUsrToken', '')
              .then(() => {
                changeOnlineStatus(validUserStore.validUser.id, { state: false })
                .then(() => {
                  validUserStore.isUserLogged = false
                  validUserStore.setValidUser(null)
                  .then(() => {
                    this.props.nav.replace({ 
                      name: 'signInScreen' 
                    })
                  })
                  .catch((err) => console.log(err))
                })
                .catch((err) => console.log(err))
              })
            }
          } else if(this.props.section.name === 'Profile' ) {
            if(subSection === 'Change Password') {
              this.props.nav.replace({
                name: 'changePasswordScreen'
              })
            } else {
              this.props.nav.replace({
                name: 'myProfile'
              })
            }
          } else if(this.props.section.name === 'Diaries') {
            getUserDiary(validUserStore.validUser.id)
            .then((diary) => {
              this.props.nav.replace({
                name: 'notes',
                passProps: {
                  market: subSection === 'Forex Diary' ? 'Fx' : 'BO',
                  notes: diary.notes
                }
              })
            })
            .catch((err) => console.los(err))
          } else if(this.props.section.name === 'Contacts') {
            this.props.nav.replace({
              name: 'myContacts'
            })
          }
        } }
        key={ index }
        >
        <Text style={ styles.subSectionTxt }>{ subSection }</Text>
       </TouchableOpacity>
    )

    return(
      <View>
        <TouchableOpacity 
          style={ styles.container }
          onPress={ () => this.setState({ isSubMenuOpen: !this.state.isSubMenuOpen }) }
        >
          <View style={ styles.symbolIcon }>
            <IconAwesome 
              name={ this.props.section.symbol }
              size={ 20 }
              color='#FFF'
            />
          </View>
          <Text style={ styles.sectionName }>{ this.props.section.name }</Text>
          <Icon 
            name={ this.state.isSubMenuOpen ? 'ios-arrow-up' : 'ios-arrow-down' }
            size={ 45 }
            color='#3399FF'
            style={ styles.sectionIcon }
          />
        </TouchableOpacity>
        <View style={ [styles.subSections, this.state.isSubMenuOpen ?  '' : { display: 'none' }] }>
          { subSections }
        </View>
      </View>
    )
  }
}

export default observer(Section);

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#444444',
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: '#fff'

  },
  sectionName: {
    fontFamily: 'Avenir',
    color: '#fff',
    fontSize: 26,
    textAlign: 'center',
    flex: 5
  },
  sectionIcon: {
    flex: 1
  },
  symbolIcon: {
    flex: 1,
    alignItems: 'flex-end'
  },
  subSectionTxt: {
    color: '#FFF',
    margin: 5,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Avenir'
  },
  subSections: {
    backgroundColor: '#282828'
  }
})