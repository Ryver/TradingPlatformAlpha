import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import { signUp } from '../../ApiActions/userRequests';

class SignUpForm extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.register = this.register.bind(this);

    this.state = {
      email: null,
      username: null,
      password: null,
      confPassword: null
    }
  }

  register(_email, _username, _password, confPassword) {
    if(_email, _username, _password, confPassword) {
      if(_password === confPassword) {
        signUp({
          email: _email,
          password: _password,
          username: _username
        })
        .then((response) => {
          if(response.msg === 'Success') {
            Alert.alert(
              'Success!',
              `Welcome ${ this.state.username }.`,
              [
                { text: "Lest's sign in.", onPress: () => this.props.nav.replace({ name: 'signInScreen' }) },
                { text: 'Stay here.', onPress: () => console.log('Stayed') }
              ]
            )
          } else {
            Alert.alert(
              'Failed!',
              response[0].msg
            )
          }
        })
        .catch((err) => console.log(err))
      } else {
        Alert.alert(
          'Failed!',
          'Passwords does not match.'
        )
      }
    } else {
      Alert.alert(
        'Failed!',
        'All field are required.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.title }>Join Us!</Text>

        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='E-mail'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('email', e.nativeEvent.text) }
          value={ this.state.email }
        />
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder="Username (can't be change)"
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('username', e.nativeEvent.text) }
          value={ this.state.username }
        />
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='Password'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          secureTextEntry
          onChange={ (e) => this.onChange('password', e.nativeEvent.text) }
          value={ this.state.password }
        />
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='Confirm Password'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          secureTextEntry
          onChange={ (e) => this.onChange('confPassword', e.nativeEvent.text) }
          value={ this.state.confPassword }
        />
        <TouchableOpacity
          style={ styles.signUpBtn }
          onPress = { () => this.register(
            this.state.email,
            this.state.username,
            this.state.password,
            this.state.confPassword
          ) }
        >
          <Text style={ styles.btnTxt }>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={ () => this.props.nav.replace({ name: 'signInScreen' }) }>
            <Text style={ styles.bottomBtnTxt }>Already Account?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default SignUpForm;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontFamily: 'Avenir',
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#fff',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#444444',
    height: 40,
    paddingLeft: 10
  },
  signUpBtn: {
    backgroundColor: '#3399FF',
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6
  },
  btnTxt: {
    color: '#fff',
    fontWeight: 'bold'
  },
  bottomBtnTxt: {
    color: '#CC6600',
    fontWeight: 'bold',
    marginTop: 10
  }
})