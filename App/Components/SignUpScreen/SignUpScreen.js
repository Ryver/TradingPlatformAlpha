import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

import SignUpForm from './SignUpForm';

class SignUpScreen extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <View style={ styles.logoContainer }>
          <Image
            source={ require('../../Styles/Images/TradingAppLogo.png')  }
            style={ styles.logo }
          />
        </View>
        <View style={ styles.formContainer }>
          <SignUpForm nav={ this.props.navigator }/>
        </View>
      </View>
    );
  }
}

export default SignUpScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  logoContainer: {
    flex: 1
  },
  logo: {
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
    flex: 1
  },
  formContainer: {
    flex: 1
  }
})