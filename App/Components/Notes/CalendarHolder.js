import React, { Component } from "react";
import Calendar from 'react-native-calendar';

const styles = {
  calendarContainer: {
    backgroundColor: '#353535'
  },
  day: {
    color: '#FFF'
  },
  selectedDayCircle: {
    backgroundColor: '#3399FF'
  },
  weekendDayButton: {
    backgroundColor: '#282828'
  },
  weekendDayText: {
    color: '#585858'
  },
  dayHeading: {
    color: '#FFF'
  },
  currentDayText: {
    color: '#CC6000'
  },
  monthContainer: {
    backgroundColor: '#282828'
  },
  currentDayCircle: {
    backgroundColor: '#CC6000'
  },
  eventIndicator: {
    backgroundColor: '#CC6000',
    width: 10,
    height: 10
  },
  controlButtonText: {
    color: '#3399FF'
  },
  title: {
    color: '#FFF'
  }
}

class CalendardHolder extends Component {

  constructor(props) {
    super(props);

    this.selectDate = this.selectDate.bind(this);
    this.onTouchNext = this.onTouchNext.bind(this);
    
    this.state = {
      datesWithPosts: [],
      months: [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 
        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ]

    }
  }

  onTouchNext(e) {
    this.state.months.map((month, index) => {
      if(e._d.toString().slice(4, 7) === month) {
        if((index + 1).toString().length == 1) {
          this.props.setSelectedMonth(`${ e._d.toString().slice(11, 15) }-0${ (index + 1).toString() }`)
        } else {
          this.props.setSelectedMonth(`${ e._d.toString().slice(11, 15) }-${ (index + 1).toString() }`)
        }
      }
    })
  }

  componentDidMount() {
    this.props.notes.map((note) => {
      this.state.datesWithPosts.push(note.openDate)
    })
  }

  selectDate(date) {
    this.props.nav.push({
      name: 'note',
      title: date.slice(0, 10),
      passProps: {
        date: date.slice(0, 10),
        notes: this.props.notes
      }
    })
  }

  render() {
    return(
      <Calendar 
        customStyle={ styles } 
        showEventIndicators
        eventDates={ this.state.datesWithPosts }
        nexButtonText={ 'Next' }
        prevButtonText={ 'Prev' }
        showControls
        onDateSelect={ (date) => this.selectDate(date) }
        onTouchNext={ (e) => this.onTouchNext(e) }
        onTouchPrev={ (e) => this.onTouchNext(e) }
      />
    )
  }
}

export default CalendardHolder;