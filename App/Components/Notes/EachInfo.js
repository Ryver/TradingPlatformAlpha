import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

class EachInfo extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.infoTitle }>{ this.props.infoTitle }</Text>
        <Text style={ [
          styles.infoVal,
          { color: this.props.valColor }
          ] }>{ this.props.infoVal }</Text>
      </View>
    )
  }
}

export default EachInfo;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 5
  },
  infoTitle: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 12
  },
  infoVal: {
    fontFamily: 'Avenir',
    fontSize: 12
  }
})