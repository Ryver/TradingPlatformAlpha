import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Picker
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import mainNavBar from '../../GlobalState/MainNavBar';

import CustomNavBar from '../CustomNavBar/CustomNavBar';
import EachInfo from './EachInfo';
import CalendarHolder from './CalendarHolder';

class Notes extends Component {

  constructor(props) {
    super(props);

    this.setSelectedMonth = this.setSelectedMonth.bind(this);
    this.setMonthlyStats = this.setMonthlyStats.bind(this);

    this.state = {
      selectedMonth: '2017-08',
      allPositions: [],
      profitPoisitons: [],
      lossPositions: []
    }
  }

  setMonthlyStats(date) {

    this.props.notes.map((note) => {
      if(note.openDate.slice(0, 7) === date ) {
        this.state.allPositions.push(note)
        if(note.balance > 0) {
         this.state.profitPoisitons.push(note)
        } else {
         this.state.lossPositions.push(note)
        }
      }
    })

  }

  setSelectedMonth(val) {
    this.setState({
      selectedMonth: val,
      allPositions: [],
      profitPoisitons: [],
      lossPositions: []
    })
    console.log(this.state.selectedMonth)
    this.setMonthlyStats()
  }

  componentWillMount() {
    mainNavBar.isNavBarVisible = false
    
    this.setMonthlyStats()
  }

  render() {
    this.setMonthlyStats(this.state.selectedMonth)
    const informations = [
      [
        {
          title: 'All Positions: ',
          val: this.state.allPositions.length,
          color: '#3399FF'
        },
        {
          title: 'Profit positions: ',
          val: this.state.profitPoisitons.length,
          color: '#48B045'
        },
        {
          title: 'Loss Positions: ',
          val: this.state.lossPositions.length,
          color: '#B80000'
        }
      ],
      [
        {
          title: 'All Positions: ',
          val: 20,
          color: '#3399FF'
        },
        {
          title: 'Win Positions: ',
          val: 17,
          color: '#48B045'
        },
        {
          title: 'Loss Positions: ',
          val: 3,
          color: '#B80000'
        }
      ]
    ]

    const displayInfo = informations[this.props.market === 'Fx' ? 0 : 1].map((info, index) =>
      <EachInfo 
        infoTitle={ info.title }
        infoVal={ info.val }
        valColor={ info.color }
        key={ index }
      />
    )

    return(
      <View style={ styles.container }>
        <CustomNavBar title='Diary' />
        <View style={ styles.datesContainer }>
          <CalendarHolder 
            nav={ this.props.navigator } 
            notes={ this.props.notes }
            setSelectedMonth={ this.setSelectedMonth }
          />
        </View>

        <View style={ styles.statsContainer }>

          <View style={ styles.btnContainer }>
            <View style={ styles.tfBtn } >
              <Text style={ styles.btnTxt }>Monthly Stats</Text>
            </View>
          </View>
          <View style={ styles.infoContainers }>
            { displayInfo }
          </View>
        </View>
      </View>
    )
  }
}

export default Notes;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  datesContainer: {
    flex: 1.2,
    backgroundColor: '#282828'
  },
  statsContainer: {
    flex: 0.75,
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  tfBtn: {
    borderColor: '#FFF',
    borderWidth: 0.5,
    height: 30,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },
  btnContainer: {
    paddingLeft: 30,
    paddingRight: 30
  },
  addBtn: {
    alignSelf: 'center'
  },
  btnTxt: {
    flex: 5,
    textAlign: 'center',
    fontFamily: 'Avenir',
    color: '#3399FF',
    fontSize: 16
  },
  btnIcon: {
    flex: 1
  },
  listContainer: {
    position: 'absolute',
    top: 40,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    backgroundColor: '#353535',
    zIndex: 99
  },
  infoContainers: {
    alignItems: 'center'
  }
})