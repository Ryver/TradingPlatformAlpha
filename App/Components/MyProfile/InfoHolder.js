import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

class InfoHolder extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.infoTitle }>{ this.props.title }</Text>
          <View style={ styles.inputContainer }>
            <TextInput 
              style={ styles.input }
              placeholder={ this.props.info }
              placeholderTextColor='#FFF'
              underLineColorAndroid='transparent'
              onChange={ (e) => this.props.onChange(this.props.field, e.nativeEvent.text) }
              value={ this.props.val }
            />
          </View>
      </View>
    )
  }
}

export default InfoHolder;

const styles = StyleSheet.create({
  infoTitle: {
    fontSize: 18,
    color: '#3399FF'
  },
  input: {
    height: 40,
    fontSize: 16,
    paddingLeft: 10,
    color: '#FFF'
  },
  inputContainer: {
    height: 40,
    borderBottomWidth: 2,
    borderColor: '#282828'
  },
  container: {
    marginBottom: 10
  }
})