import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { editProfile } from '../../ApiActions/userRequests';
import mainNavBar from '../../GlobalState/MainNavBar';
import io from 'socket.io-client';

const socket = io.connect('http://localhost:3020');

import CustomNavBar from '../CustomNavBar/CustomNavBar';
import InfoHolder from './InfoHolder';

class MyProfile extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.saveChanges = this.saveChanges.bind(this);

    this.state = {
      userAvatar: null,
      email: null,
      firstName: null,
      lastName: null,
      myFxBook: null,
      ownWebsite: null,
      tradingExp: null
    }
  }

  componentWillMount() {
    mainNavBar.isNavBarVisible = false
  }

  saveChanges() {
    editProfile(this.props.user.id, {
      email: this.state.email ? this.state.email : this.props.user.email,
      firstName: this.state.firstName ? this.state.firstName : this.props.user.firstName,
      lastName: this.state.lastName ? this.state.lastName : this.props.user.lastName,
      myFxBook: this.state.myFxBook ? this.state.myFxBook : this.props.user.myFxBook,
      ownWebsite: this.state.ownWebsite ? this.state.ownWebsite : this.props.user.ownWebsite,
      expirience: this.state.tradingExp ? this.state.tradingExp : this.props.user.expirience
    })
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {

    const informations = [
      {
        title: 'E-mail',
        info: this.props.user.email,
        field: 'email'
      },
      {
        title: 'First name',
        info: this.props.user.firstName,
        field: 'firstName'
      },
      {
        title: 'Last name',
        info: this.props.user.lastName,
        field: 'lastName'
      },
      {
        title: 'MyFxBook',
        info: this.props.user.myFxBook,
        field: 'myFxBook'
      },
      {
        title: 'Own website',
        info: this.props.user.ownWebsite,
        field: 'ownWebsite'
      },
      {
        title: 'Trading expirience',
        info: this.props.user.expirience,
        field: 'tradingExp'
      }
    ]

    const displayInformations = informations.map((info, index) =>
      <InfoHolder 
        title={ info.title }
        field={ info.field }
        onChange={ this.onChange }
        info={ info.info }
        val={ this.state[info.field] }
        key={ index }
      />
    )

    const avatar = this.state.userAvatar ?
    <Image 
      source={ require(this.state.userAvatar) }
    />
    :
    <Icon
      name='ios-add'
      size={ 60 }
      color='#CC6000'
    />

    return(
      <View style={ styles.container }>
        <CustomNavBar
          title='My Profile'
          onClick={ () => this.saveChanges() }
        />
        <View style={ styles.upperPart }>
          <TouchableOpacity style={ styles.avatar }>
            {avatar}
          </TouchableOpacity>
          <Text style={ styles.username }>{ this.props.user.username }</Text>
        </View>
        <View style={ styles.profileInfo }>
          <ScrollView>
            { displayInformations }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default MyProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 20
  },
  upperPart: {
    flex: 1,
    backgroundColor: '#282828',
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatar: {
    backgroundColor: '#353535',
    borderRadius: 100,
    width: 80,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#979797',
    marginBottom: 10
  },
  username: {
    color: '#3399FF',
    fontFamily: 'Avenir',
    fontSize: 30
  },
  profileInfo: {
    flex: 2,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20
  }
})