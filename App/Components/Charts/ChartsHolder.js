import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import CustomNavBar from '../CustomNavBar/CustomNavBar';

class ChartsHolder extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <CustomNavBar title='Fx Market' />
        <View style={ styles.topBar }>

          <TouchableOpacity 
            style={ styles.leftBtnHolder }
            onPress={ () => console.log('Options') }
          >
            <Icon 
              name='ios-arrow-back'
              size={ 25 }
              color='#CC6000'
            />
            <Text style={ [
              styles.btnTxt,
              { marginLeft: 5 }
              ] }>Options</Text>
          </TouchableOpacity>

          <TouchableOpacity 
            style={ styles.centerBtnHolder }
            onPress={ () => console.log('Select stock') }
          >
            <Text style={ styles.stockTxt }>Eur/Usd</Text>
            <Icon 
              name='ios-arrow-down'
              size={ 35 }
              color='#CC6000'
            />
          </TouchableOpacity>

          <TouchableOpacity 
            style={ styles.rightBtnHolder }
            onPress={ () => console.log('Position') }
          >
            <Text style={ [
              styles.btnTxt,
              { marginRight: 5 }
            ] }>Position</Text>
            <Icon 
              name='ios-arrow-forward'
              size={ 25 }
              color='#CC6000'
            />
          </TouchableOpacity>

        </View>
        <View style={ styles.chart}>
          <Text style={ styles.chartTxt }>CHART</Text>
        </View>
      </View>
    )
  }
}

export default ChartsHolder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  },
  topBar: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    height: 40
  },
  leftBtnHolder: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  centerBtnHolder: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rightBtnHolder: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  btnTxt: {
    color: '#CC6000',
    fontFamily: 'Avenir',
    fontSize: 14
  },
  stockTxt: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 5
  },
  chart: {
    backgroundColor: '#FFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  chartTxt: {
    fontSize: 100,
    fontWeight: 'bold'
  }
})