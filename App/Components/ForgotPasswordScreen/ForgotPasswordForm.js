import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';

class ForgotPasswordForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: null
    }
  }

  render(){
    return(
      <View style={ styles.container }>
        <Text style={ styles.title }>Get New Password</Text>

        <TextInput
          style={ styles.input }
          placeholder='E-mail'
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.setState({ email: e.nativeEvent.text }) }
          value={ this.state.email }
        />
        <TouchableOpacity
          style={ styles.sendBtn }
          onPress = { () => console.log('Sign Up') }
        >
          <Text style={ styles.btnTxt }>Send</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={ () => this.props.navigator.replace({ name: 'signInScreen' }) }>
            <Text style={ styles.bottomBtnTxt }>Cancel</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ForgotPasswordForm;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontFamily: 'Avenir',
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#fff',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#444444',
    height: 40,
    paddingLeft: 10
  },
  sendBtn: {
    backgroundColor: '#3399FF',
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6
  },
  btnTxt: {
    color: '#fff',
    fontWeight: 'bold'
  },
  bottomBtnTxt: {
    color: '#CC6600',
    fontWeight: 'bold',
    marginTop: 10
  }
})