import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ScrollView
} from 'react-native';
import mainNavBar from '../../GlobalState/MainNavBar';
import appSettingsStore from '../../GlobalState/AppSettingsStore';

import CustomNavBar from '../CustomNavBar/CustomNavBar';
import Setting from './Setting';

class SettingsView extends Component {

  componentWillMount() {
    mainNavBar.isNavBarVisible = false
  }

  render() {
    return(
      <View style={ styles.container }>
        <CustomNavBar title='Settings'/>
        <ScrollView>
          <View style={ [styles.settingsSection, { marginTop: 20 }] }>
            <Text style={ styles.sectionTitle }>Notifications Settings</Text>
            <Setting 
              storageContainer='notificationsDisplay'
              isEnable={ appSettingsStore.notificationsDisplay }
              title='Display Notificaitons' 
            />
            <Setting 
              storageContainer='notificationsVibrate'
              isEnable={ appSettingsStore.notificationsVibrate }
              title='Vibrations' 
            />
            <Setting 
              storageContainer='notificationsSounds'
              isEnable={ appSettingsStore.notificationsSounds }
              title='Sounds' 
            />
          </View>
          <View style={ [styles.settingsSection, { marginTop: 20 }] }>
            <Text style={ styles.sectionTitle }>Diaries Settings</Text>
            <Setting 
              storageContainer='diariesAutoSave'
              isEnable={ appSettingsStore.diariesAutoSave }
              title='Auto Save' 
            />
            <Setting 
              storageContainer='diariesSavePosSize'
              isEnable={ appSettingsStore.diariesSavePosSize }
              title='Save Position Size' 
            />
            <Setting 
              storageContainer='diariesPairsName'
              isEnable={ appSettingsStore.diariesPairsName }
              title='Save Pairs Name' 
            />
            <Setting 
              storageContainer='sharingDiary'
              isEnable={ appSettingsStore.sharingDiary }
              title='Sharing Diary' 
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default SettingsView;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  settingsSection: {
    paddingLeft: 20,
    paddingRight: 20
  },
  sectionTitle: {
    color: '#3399FF',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 22
  }
})