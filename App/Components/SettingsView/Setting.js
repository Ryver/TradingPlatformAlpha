import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import appSettingsStore from '../../GlobalState/AppSettingsStore';

class Setting extends Component {

  constructor(props) {
    super(props);

    this.changeState = this.changeState.bind(this);

    this.state = {
      isEnable: this.props.isEnable
    }
  }
  changeState() {
    this.setState({ isEnable: !this.state.isEnable })
    AsyncStorage.getItem('TradingAppSettings')
    .then((result) => {
      const res = JSON.parse(result);

      res[this.props.storageContainer] = this.state.isEnable
      AsyncStorage.setItem('TradingAppSettings', JSON.stringify(res))
      appSettingsStore.settingsObj();
    })
  }

  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.settingTitle }>{ this.props.title }</Text>
        <TouchableOpacity 
          style={ styles.btn }
          onPress={ () => this.changeState() }
        >
          <View style={ [
            styles.halfBtn,
            this.state.isEnable ? { backgroundColor: '#3399FF' } : { backgroundColor: '#282828' }
          ] }>
            <Text style={ styles.halfBtnTxt }>ON</Text>
          </View>
          <View style={ [
            styles.halfBtn,
            this.state.isEnable ? { backgroundColor: '#282828' } : { backgroundColor: '#3399FF' }
          ] }>
            <Text style={ styles.halfBtnTxt }>OFF</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default Setting;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 45,
    paddingLeft: 10,
    borderBottomWidth: 2,
    borderColor: '#282828',
    marginBottom: 20
  },
  settingTitle: {
    flex: 4,
    alignSelf: 'center',
    fontFamily: 'Avenir',
    color: '#FFF',
    fontSize: 18
  },
  btn: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 20
  },
  halfBtn: {
    height: 30,
    width: 30,
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#FFF'
  },
  halfBtnTxt: {
    fontSize: 12,
    color: '#FFF',
    textAlign: 'center'
  }
})