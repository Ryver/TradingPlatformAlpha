import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import loadingScreenState from '../../GlobalState/LoadingScreenState';

class LoadingScreen extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Spinner visible={ loadingScreenState.isLoading } color='#CC6600' size='large' />
      </View>
    )
  }
}

export default observer(LoadingScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  }
})