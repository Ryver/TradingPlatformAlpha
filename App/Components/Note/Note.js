import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import mainNavBar from '../../GlobalState/MainNavBar';
import Icon from 'react-native-vector-icons/Ionicons';

import Post from './Post';

class Note extends Component {

  constructor(props) {
    super(props);

    this.goToAddPost = this.goToAddPost.bind(this);

    this.state = {
      notes: []
    }
  }

  goToAddPost() {
    this.props.navigator.push({
      name: 'newPost',
      title: 'New Post',
      passProps: {
        date: this.props.date
      }
    })
  }

  componentWillMount() {
    mainNavBar.isNavBarVisible = true
    this.props.notes.map((note) => {
      if(note.openDate === this.props.date) {
        this.state.notes.push(note)
      }
    })
  }

  render() {

    const displayPosts = this.state.notes.map((post, index) => 
      <Post 
        post={ post }
        key={ index } 
      />
    )

    return(
      <View style={ styles.container }>
        <TouchableOpacity 
          style={ styles.addBtn } 
          onPress={ () => this.goToAddPost() }
        >
          <Icon 
            name='ios-add'
            size={ 45 }
            color='#3399FF'
          />

        </TouchableOpacity>
        <ScrollView>
          { displayPosts }
        </ScrollView>
      </View>
    )
  }
}

export default Note;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60
  },
  addBtn: {
    alignItems: 'center'
  }
})