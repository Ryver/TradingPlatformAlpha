import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

class Post extends Component {
  render() {
    return(
      <View style={ styles.container }>

        <View style={ styles.header }>
          <Text style={ styles.headerTitle }>{ this.props.post.currencyPair }</Text>
          <View style={ styles.balanceContainer }>
            <Text style={ [
              styles.balance,
              { backgroundColor: this.props.post.balance >= 0 ? '#2F940C' : 'red' }
            ] }>{ this.props.post.balance }</Text>
          </View>
        </View>

        <View style={ styles.openAndClose }>

          <View style={ [
            styles.column,
            { alignItems: 'flex-start' }
          ] }>
            <Text style={ styles.columnDate }>{ this.props.post.openDate }</Text>
          </View>

          <View style={ [
            styles.column,
            { alignItems: 'flex-end' }
          ] }>
            <Text style={ styles.columnDate }>{ this.props.post.closeDate === 'null-null-null' ? '' : this.props.post.closeDate }</Text>
          </View>
        </View>

        <View style={ styles.commentContainer }>
          <Text style={ styles.comment }>{ this.props.post.comment }</Text>
        </View>

      </View>
    )
  }
}

export default Post;

const styles = StyleSheet.create({
  container: {
    marginBottom: 10
  },
  header: {
    backgroundColor: '#282828',
    paddingLeft: 20,
    height: 35,
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerTitle: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    flex: 1
  },
  balanceContainer: {
    height: 35,
    justifyContent: 'center'
  },
  balance: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 20,
    paddingLeft: 5,
    paddingTop: 7.5,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  openAndClose: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingHorizontal: 20,
    opacity: 0.5
  },
  column: {
    flex: 1
  },
  columnDate: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 18,
  },
  columnHour: {
    fontFamily: 'Avenir',
    fontSize: 16,
    color: '#FFF'
  },
  commentContainer: {
    paddingHorizontal: 20,
    paddingTop: 10
  },
  comment: {
    fontFamily: 'Avenir',
    fontSize: 18,
    color: '#FFF'
  }
})