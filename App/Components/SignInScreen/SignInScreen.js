import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

import SignInForm from './SignInForm';

class SignInScreen extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <View style={ styles.imageContainer }>
          <Image
            source={ require('../../Styles/Images/WelcomeScrImg.png') }
            style={ styles.welcomeScrImg }
          >
           <View style={ styles.styling } />
            <Image
              source={ require('../../Styles/Images/TradingAppLogo3.png') }
              style={ styles.logo }
            />
          </Image>
        </View>
        <View style={ styles.formContainer }>
          <SignInForm navigator={ this.props.navigator } />
        </View>
      </View>
    )
  }
}

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imageContainer: {
    flex: 2,
    paddingTop: 20,
  },
  welcomeScrImg: {
    flex: 1,
    height: undefined,
    width: undefined,
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    opacity: 0.7
  },
  formContainer: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  logo: {
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
    flex: 1,
  },
  styling: {
    flex: 1
  }
})