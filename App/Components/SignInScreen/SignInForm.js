import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { signIn } from '../../ApiActions/userRequests';
import validUserStore from '../../GlobalState/ValidUserStore';
import { observer } from 'mobx-react';
import loadingScreenState from '../../GlobalState/LoadingScreenState';

class SignInForm extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.logIn = this.logIn.bind(this);

    this.state = {
      email: null,
      password: null
    }
  }

  logIn(_email, _password) {
    if(_email, _password) {
      signIn({
        email: _email,
        password: _password
      })
      .then((res) => {
        if(res.msg === 'Success') {
          this.setState({
            email: null,
            password: null
          })
          AsyncStorage.setItem('TradingAppUsrToken', res.token)
          loadingScreenState.isLoading = true
          validUserStore.setValidUser(res.token)
          .then(() => {
            this.props.navigator.replace({ name: 'myProfile' })
          })
          .catch((err) => console.log(err))
        } else {
          Alert.alert(
            'Failed!',
            res.msg
          )
        }
      })
      .catch((err) => console.log(err))
    } else {
      Alert.alert(
        'Failed!',
        'All fileds are required.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>
        <View style={ styles.inputField }>
          <Icon
            name='envelope'
            size={ 20 }
            color='#ADADAD'
            style={ styles.icon }
          />
          <TextInput
            style={ styles.input }
            placeholder='E-mail'
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            onChange={ (e) => this.onChange('email', e.nativeEvent.text) }
            value={ this.state.email }
          />
        </View>
        <View style={ [styles.inputField, { borderTopWidth: 0 }] }>
          <Icon
            name='lock'
            size={ 20 }
            color='#ADADAD'
            style={ styles.icon }
          />
          <TextInput
            style={ styles.input }
            placeholder='Password'
            secureTextEntry
            placeholderTextColor='#ADADAD'
            underlineColorAndroid='transparent'
            onChange={ (e) => this.onChange('password', e.nativeEvent.text) }
            value={ this.state.password }
          />
        </View>
        <TouchableOpacity
          style={ styles.signInBtn }
          onPress = { () => this.logIn(
            this.state.email,
            this.state.password
          ) }
        >
          <Text style={ styles.btnTxt }>Sign In</Text>
        </TouchableOpacity>

        <View style={ styles.bottomButtons }>
          <TouchableOpacity
            style={ styles.bottomBtn }
            onPress={ () => this.props.navigator.replace({ name: 'forgotPasswordScreen' }) }
          >
            <Text style={ styles.btnTxt }>Forgot Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={ styles.bottomBtn }
            onPress={ () => this.props.navigator.replace({ name: 'signUpScreen' }) }
          >
            <Text style={ [styles.btnTxt, { textAlign: 'right' }] }>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default observer(SignInForm);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputField: {
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#444444',
    height: 40,
    justifyContent: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
  },
  input: {
    flex: 10,
    fontFamily: 'Avenir',
    color: '#fff',
  },
  signInBtn: {
    backgroundColor: '#3399FF',
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6
  },
  btnTxt: {
    color: '#fff',
    fontWeight: 'bold'
  },
  bottomButtons: {
    paddingTop: 10,
    flex: 1,
    flexDirection: 'row'
  },
  bottomBtn: {
    flex: 1
  },
  icon: {
    alignSelf: 'center',
    flex: 1
  }
})