import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import globalNavStore from '../../GlobalState/GlobalNavStore';

class MenuIcon extends Component {
  render() {
    return(
      <TouchableOpacity 
        style={ styles.container }
        onPress={ () => globalNavStore.isNavOpen = true }
      > 
        <View style={ styles.line } />
        <View style={ styles.line } />
        <View style={ styles.line } /> 
      </TouchableOpacity>
    )
  }
}

export default MenuIcon;

const styles = StyleSheet.create({
  container: {
    height: 40,
    paddingTop: 6
  },
  line: {
    width: 40,
    height: 3,
    backgroundColor: '#3399FF',
    marginBottom: 8
  }
})