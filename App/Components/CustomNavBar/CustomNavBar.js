import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform
} from 'react-native';

import MenuIcon from './MenuIcon';

class CustomNavBar extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <View
          style={ styles.menuBtn }
        >
          <MenuIcon />
        </View>
        <View style={ styles.sectionTitle }>
          <Text style={ styles.title}>{ this.props.title }</Text>
        </View>        
        <TouchableOpacity style={ [
          styles.rightBtn,
          this.props.onClick ? '' : { display: 'none' }
          ] }
          onPress={ () => this.props.onClick() }
          >
          <Text style={ styles.rightBtnTxt }>Save</Text>
        </TouchableOpacity>
        <View style={ [
          { flex: 1 },
          this.props.onClick ? { display: 'none' } : ''
        ] } />
      </View>
    )
  }
}

export default CustomNavBar;

const styles = StyleSheet.create({
  container: {
    height: 38,
    backgroundColor: '#555555',
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: '#979797',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    marginTop: Platform.OS === 'android' ? 0 : 20
  },
  menuBtn: {
    flex: 1
  },
  sectionTitle: { 
    flex: 5
  },
  title: {
    textAlign: 'center',
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold'
  },
  rightBtn: {
    flex: 1
  },
  rightBtnTxt: {
    fontSize: 16,
    textAlign: 'right',
    color: '#3399FF',
    fontFamily: 'Avenir'
  }
})