import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import mainNavBar from '../../GlobalState/MainNavBar';
import { observer } from 'mobx-react';

import CalculatorBtn from './CalculatorBtn';
import CustomNavBar from '../CustomNavBar/CustomNavBar';

class CalculatorsListView extends Component {

  componentDidMount() {
    mainNavBar.isNavBarVisible = false
  }

  render() {
    const calculatorBtns = [
      [
        'Position Size (% risk)',
        'Position Size ($ risk)',
        'Stop Loss',
        'Profit Calculator'
      ],
      [
        'Strike Profit'
      ],
      [
        'Stocks Profit Calculator'
      ]
    ]

    const buttons = calculatorBtns[this.props.sectionIndex].map((btn, index) =>
      
      <CalculatorBtn
        title={ btn }
        key={ index }
        nav={ this.props.navigator }
      />
    )

    return(
      <View style={ styles.container }>
        <CustomNavBar title={ this.props.sectionName } />
        <ScrollView>{ buttons }</ScrollView>
      </View>
    )
  }
}

export default CalculatorsListView;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    fontFamily: 'Avenir',
    color: '#3399FF',
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20
  }
})