import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import mainNavBar from '../../GlobalState/MainNavBar';

class CalculatorBtn extends Component {

  constructor(props) {
    super(props);

    this.goToCalculator = this.goToCalculator.bind(this);
  }
  

  goToCalculator() {
    mainNavBar.isNavBarVisible = true
    if( this.props.title === 'Stop Loss' ) {
      this.props.nav.push({
        name: 'fxStopLoss',
        title: 'Stop Loss Calculator'
      })
    } else if(this.props.title === 'Position Size (% risk)' || this.props.title === 'Position Size ($ risk)') {
      this.props.nav.push({
        name: 'fxPositionSize',
        title: 'Position Size Calculator',
        passProps: {
          riskType: this.props.title == 'Position Size (% risk)' ? '%' : ''
        }
      })
    } else if(this.props.title === 'Profit Calculator') {
      this.props.nav.push({
        name: 'fxProfit',
        title: 'Profit Calculator'
      })
    } else if(this.props.title === 'Strike Profit') {
      this.props.nav.push({
        name: 'boStrike',
        title: 'Strike Calculator'
      })
    } else if(this.props.title ==='Stocks Profit Calculator') {
      this.props.nav.push({
        name: 'stocksProfit',
        title: 'Profit Calculator'
      })
    }
  }

  render() {
    return(
      <TouchableOpacity 
        style={ styles.container }
        onPress={ () => this.goToCalculator() }
      >
      <View style={{ flex: 1 }} />
      <Text style={ styles.title }>{ this.props.title }</Text>
      <Icon 
        name='ios-arrow-forward'
        size={ 45 }
        color='#3399FF'
        style={ styles.calculatorBtnIcon }
      />
      </TouchableOpacity>
    )
  }
}

export default CalculatorBtn;

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#282828',
    paddingRight: 10
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 22,
    alignSelf: 'center'
  },
  calculatorBtnIcon: {
    flex: 1,
    alignSelf: 'flex-start'
  }
})