import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import AddBtn from './Buttons/AddBtn';
import InvitedBtn from './Buttons/InvitedBtn';
import AlreadyBtn from './Buttons/AlreadyBtn';

class NewContact extends Component {
  render() {

    const displayBtn = () => {
      if(this.props.contact.msg === 'Invite') {
        return <AddBtn contactUsername={ this.props.contact.username } />
      } else if(this.props.contact.msg === 'Already Contact') {
        return <AlreadyBtn />
      } else {
        return <InvitedBtn />
      }
    }

    console.log(this.props.contact)
    return(
      <View style={ styles.container }>
        <View style={ styles.avatarContainer }>
          <Icon 
            name='ios-person'
            size={ 46 }
            color='#3399FF'
          />
        </View>
        <View style={ styles.usernameContainer }>
          <Text style={ styles.username }>{ this.props.contact.username }</Text>
          { displayBtn() }
        </View>
      </View>
    )
  }
}

export default NewContact;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  username: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 22,
    paddingLeft: 20
  }
})