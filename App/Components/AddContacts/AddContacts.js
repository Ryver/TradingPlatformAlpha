import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions
} from 'react-native';
import { observer } from 'mobx-react';

import AddContactsPanel from './AddContactsPanel'

class AddContactsModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      componentPosition: new Animated.Value(Dimensions.get('window').width),
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps.isAddContactFormOpen)
    if(nextProps.isAddContactFormOpen !== this.props.isAddContactFormOpen) {
      Animated.timing(
        this.state.componentPosition, {
          toValue: nextProps.isAddContactFormOpen ? 0 : Dimensions.get('window').width,
          duration: 300
        }
      ).start()
    }
  }

  render() {
    return(
      <Animated.View style={ [
        styles.container,
        { right: this.state.componentPosition }
      ] }>
        <AddContactsPanel />
      </Animated.View>
    )
  }
}

export default observer(AddContactsModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.3)'
  }
})