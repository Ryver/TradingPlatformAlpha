import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

class AlreadyBtn extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.btnTxt }>Already Contact</Text>
      </View>
    )
  }
}

export default AlreadyBtn;

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginTop: 10,
    backgroundColor: '#008000',
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    paddingHorizontal: 10
  },
  btnTxt: {
    color: '#FFF',
    fontFamily: 'Avenir'
  }
})