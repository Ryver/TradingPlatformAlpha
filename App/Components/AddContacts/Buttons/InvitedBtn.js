import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

class InvitedBtn extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <Text style={ styles.btnTxt }>Invited...</Text>
      </View>
    )
  }
}

export default InvitedBtn;

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginTop: 10,
    backgroundColor: '#3399FF',
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    opacity: 0.7,
    paddingHorizontal: 10
  },
  btnTxt: {
    color: '#FFF',
    fontFamily: 'Avenir'
  }
})