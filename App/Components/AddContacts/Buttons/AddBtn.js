import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert
} from 'react-native';
import { newContactReq } from '../../../ApiActions/contactRequests';
import validUserStore from '../../../GlobalState/ValidUserStore';
import io from 'socket.io-client';

const socket = io.connect('http://localhost:3020');

class AddBtn extends Component {

  constructor(props) {
    super(props);

    this.inviteToContacts = this.inviteToContacts.bind(this);
  }

  inviteToContacts(_firstUsr, _secondUsr) {
    newContactReq({
      firstUsr: _firstUsr,
      secondUsr: _secondUsr
    })
    .then((response) => {
      if(resposne.msg === 'Success') { 
        socket.emit(`invited`, { 
          invite: _secondUsr ,
          sendBy: _firstUsr
        })
      }

      Alert.alert(
        response.msg === 'Success' ? 'Success!' : 'Failed!',
        response.msg === 'Success' ? 'Contact request sent.' : 'Something gone wrong please try again.'
      )
    })
    .catch((err) => console.log(err))
  }

  render() {
    return(
      <TouchableOpacity
        style={ styles.container }
        onPress={ () => this.inviteToContacts(validUserStore.validUser.username, this.props.contactUsername) }
      >
        <Text style={ styles.btnTxt }>Invite To Contact</Text>
      </TouchableOpacity>
    )
  }
}

export default AddBtn;

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginTop: 10,
    backgroundColor: '#3399FF',
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    paddingHorizontal: 10
  },
  btnTxt: {
    color: '#FFF',
    fontFamily: 'Avenir'
  }
})