import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class SearchField extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <TextInput 
          style={ styles.input }
          placeholder='Enter Username'
          placeholderTextColor='#757575'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.props.changeVal(e.nativeEvent.text) }
        />
        <TouchableOpacity 
          style={ styles.searchIcon }
          onPress={ () => this.props.search() }
        >
          <Icon 
            name='ios-search'
            size={ 32 }
            color='#3399FF'
          />
        </TouchableOpacity>
      </View>
    )
  }
}

export default SearchField;

const styles = StyleSheet.create({
  container: {
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#353535',
    borderWidth: 0.5,
    borderColor: '#FFF',
    alignItems: 'center',
    paddingHorizontal: 10
  },
  input: {
    flex: 8,
    color: '#FFF'
  },
  searchIcon: {
    flex: 1
  }
})