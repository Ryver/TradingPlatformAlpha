import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import addContactStore from '../../GlobalState/AddContactStore';
import { contactsSearch } from '../../ApiActions/userRequests';
import validUserStore from '../../GlobalState/ValidUserStore';

import SearchField from './SearchField';
import NewContact from './Contact';

class AddContactsPanel extends Component {

  constructor(props) {
    super(props);

    this.changeVal = this.changeVal.bind(this);
    this.search = this.search.bind(this);

    this.state = {
      username: null,
      result: null
    }
  }

  search(_firstUsr, _secondUsr) {
    contactsSearch({
      firstUsr: this.state.username,
      secondUsr: validUserStore.validUser.username
    })
    .then((response) => {
      if(response.msg === 'User with this name does not exist.') {
        this.setState({ result: null })
        Alert.alert(
          'Failed!',
          response.msg
        )
      } else {
        this.setState({ result: null })
        this.setState({ result: response })
      }
    })
  }

  changeVal(val) {
    this.setState({ username: val })
  }

  render() {

    const displaySearchResult = () => {
      return this.state.result ? <NewContact contact={ this.state.result } /> : null
      
    }

    return(
      <View style={ [
        styles.container,
        addContactStore.isOpen ? '' : { display: 'none' }
        ] }>
        <View style={ styles.mainContainer }>

          <View>
            <TouchableOpacity onPress={ () => addContactStore.isOpen = false }>
              <Icon 
                name='ios-close'
                size={ 56 }
                color='#CC6000'
              />
            </TouchableOpacity>
          </View>

          <View>
            <SearchField 
              changeVal={ this.changeVal } 
              search={ this.search }
            />
          </View>

          <View style={ styles.contactsList }>
            <ScrollView>
              { displaySearchResult() }
            </ScrollView>
          </View>
        </View>
        <View style={{ flex: 1 }} />
      </View>
    )
  }
}

export default AddContactsPanel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  mainContainer: {
    flex: 3,
    backgroundColor: '#282828',
    paddingHorizontal: 20,
    paddingTop: 20
  },
  contactsList: {
    marginTop: 20
  }
})