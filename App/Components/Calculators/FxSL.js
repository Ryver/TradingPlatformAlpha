import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';
import { stopLossCalc } from '../../CalculatorsScripts/ForexCalculators';

class FxSL extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.calculate = this.calculate.bind(this);

    this.state = {
      accCurrency: 'USD',
      isAccCurrencyOpen: false,
      riskType: 'Standard',
      isRiskTypeOpen: false,
      positionSize: null,
      pair: 'USD',
      isPairOpen: false,
      risk: null,
      posType: 'sell',
      enterPrice: null,
      isLoadingOpen: false
    }
  }

  calculate(accCurrency, riskType, posSize, pair, risk) {
    if(posSize && risk && this.state.enterPrice) {
      stopLossCalc(riskType, posSize, accCurrency, pair, risk)
      .then((result) => {
        const slPrice = this.state.posType === 'buy' ?
        this.state.enterPrice - (result * 0.0001) 
        : 
        (this.state.enterPrice * 1) + (result * 0.0001)
        Alert.alert(
          'Result:',
          `SL Distance: ${ result } pips
          SL should be on: ${ slPrice.toFixed(4) }`
        )
      })
      .catch((err) => console.log(err))
    } else {
      Alert.alert(
        'Failed!',
        'Some required fields are empty.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <ScrollView style={ styles.container }>
        <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Account Currency</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isAccCurrencyOpen: !this.state.isAccCurrencyOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ this.state.accCurrency }</Text>
              <Icon 
                name={ this.state.isAccCurrencyOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.accCurrency }
              onValueChange={ (itemValue) => this.setState({ accCurrency: itemValue }) }
              style={ this.state.isAccCurrencyOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='USD' value='USD' />
              <Picker.Item color='#3399FF' label='EUR' value='EUR' />
              <Picker.Item color='#3399FF' label='GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='JPY' value='JPY' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Risk Type</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isRiskTypeOpen: !this.state.isRiskTypeOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `${ this.state.riskType } Lots` }</Text>
              <Icon 
                name={ this.state.isRiskTypeOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.riskType }
              onValueChange={ (itemValue) => this.setState({ riskType: itemValue }) }
              style={ this.state.isRiskTypeOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='Standard Lots' value='Standard' />
              <Picker.Item color='#3399FF' label='Micro Lots' value='Micro' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Position Size</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('positionSize', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Risk</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('risk', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Pair</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isPairOpen: !this.state.isPairOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `XXX/${ this.state.pair }` }</Text>
              <Icon 
                name={ this.state.isPairOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.pair }
              onValueChange={ (itemValue) => this.setState({ pair: itemValue }) }
              style={ this.state.isPairOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='XXX/USD' value='USD' />
              <Picker.Item color='#3399FF' label='XXX/AUD' value='AUD' />
              <Picker.Item color='#3399FF' label='XXX/GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='XXX/CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='XXX/PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='XXX/JPY' value='JPY' />
              <Picker.Item color='#3399FF' label='XXX/CAD' value='CAD' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Enter Price</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('enterPrice', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Position Type</Text>
            <View style={ styles.formFieldContainerBtn }>
              <TouchableOpacity style={ [
                styles.posTypeBtn,
                this.state.posType === 'buy' ? { backgroundColor: '#3399FF' } : ''
                ] }
                onPress={ () => this.setState({ posType: 'buy' }) }
                >
                <Text style={ styles.posTypeBtnTxt }>Buy</Text>
              </TouchableOpacity>
              <TouchableOpacity style={ [
                styles.posTypeBtn,
                this.state.posType === 'sell' ? { backgroundColor: '#3399FF' } : ''
                ] }
                onPress={ () => this.setState({ posType: 'sell' }) }
                >
                <Text style={ styles.posTypeBtnTxt }>Sell</Text>
              </TouchableOpacity>
            </View>

          <TouchableOpacity 
            style={ styles.calculateBtn }
            onPress={ () => this.calculate(
              this.state.accCurrency,
              this.state.riskType,
              this.state.positionSize,
              this.state.pair,
              this.state.risk
            ) }
          >
            <Spinner visible={ this.state.isLoadingOpen } color='#CC6600' size='large' />
            <Text style={ styles.calculateBtnTxt }>Calculate</Text>
          </TouchableOpacity>
          </View>
      </ScrollView>
    )
  }
}

export default FxSL;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingLeft: 20,
    paddingRight: 20
  },
  fieldTxt: {
    textAlign: 'center',
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  formFieldContainer: {
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#3399FF',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#353535',
    height: 40,
    paddingLeft: 10,
    textAlign: 'center'
  },
  formFieldContainerBtn: {
    flexDirection: 'row',
    backgroundColor: '#353535',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#fff'
  },
  fieldContainerBtnTxt: {
    color: '#3399FF',
    fontFamily:'Avenir',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 10,
    textAlign: 'center'
  },
  accCurrencyIcon: {
    flex: 1
  },
  posTypeBtn: {
    flex: 1,
    height: 39,
    justifyContent: 'center'
  },
  posTypeBtnTxt: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center'
  },
  calculateBtn: {
    height: 38,
    backgroundColor: '#282828',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399FF',
    marginTop: 15
  },
  calculateBtnTxt: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})