import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import * as d3 from 'd3';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  positionSizeCalcPercentRisk,
  positionSizeCalcValueRisk
} from '../../CalculatorsScripts/ForexCalculators';
import Spinner from 'react-native-loading-spinner-overlay';

class FxPositionSize extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.calculate = this.calculate.bind(this);

    this.state = {
      accountCurrency: 'USD',
      accAmount: 0,
      isAccCurrencyOpen: false,
      isRiskOpen: false,
      risk: 0,
      slDistance: null,
      isLevargeOpen: false,
      levarge: 1,
      pair: 'USD',
      isPairOpen: false,
      isLoadingOpen: false
    }
  }

  calculate(accAmount, risk, accCurrency, slDistance, pair, levarge) {
    if(slDistance) { 
      this.setState({ isLoadingOpen: true })
      if(this.props.riskType === '%') {
        positionSizeCalcPercentRisk(accAmount, risk, accCurrency, slDistance, pair, levarge)
        .then((result) =>  { 
          this.setState({ isLoadingOpen: false })
          Alert.alert(
          'Result:',
          `Units: ${ result }
          Standard Lots: ${ (result / 100000).toFixed(2) }
          Micro Lots: ${ (result / 1000).toFixed(2) }`
        ) }) 
        .catch((err) => console.log(err))
      } else {
        positionSizeCalcValueRisk(risk, accCurrency, slDistance, pair, levarge)
        .then((result) => {
          this.setState({ isLoadingOpen: false })
          Alert.alert(
          'Result:',
          `Units: ${ result }
          Standard Lots: ${ (result / 100000).toFixed(2) }
          Micro Lots: ${ (result / 1000).toFixed(2) }`
        )} )
      }  
    } else {
      Alert.alert(
        'Failed',
        'Some required fields are empty.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container } >
        <ScrollView style={ styles.fieldsContainer }>
          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Account Currency</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isAccCurrencyOpen: !this.state.isAccCurrencyOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ this.state.accountCurrency }</Text>
              <Icon 
                name={ this.state.isAccCurrencyOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.accountCurrency }
              onValueChange={ (itemValue) => this.setState({ accountCurrency: itemValue }) }
              style={ this.state.isAccCurrencyOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='USD' value='USD' />
              <Picker.Item color='#3399FF' label='EUR' value='EUR' />
              <Picker.Item color='#3399FF' label='GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='JPY' value='JPY' />
            </Picker>
          </View>

          <View style={ [
            styles.formFieldContainer, 
            this.props.riskType == '%' ? '' : { display: 'none' },
          ] }>
            <Text style={ styles.fieldTxt }>Account Ammount</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('accAmount', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Risk</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isRiskOpen: !this.state.isRiskOpen }) }
              style={ [
                styles.formFieldContainerBtn,
                this.props.riskType == '%' ? '' : { display: 'none' } 
              ] }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `${ this.state.risk }%` }</Text>
              <Icon 
                name={ this.state.isRiskOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.risk }
              onValueChange={ (itemValue) => this.setState({ risk: itemValue }) }
              style={ this.state.isRiskOpen ? '' : { display: 'none' } }
            >
              {d3.range(101).map((num, index) =>{
                if(index > 0) {
                  return <Picker.Item 
                    color='#3399FF' 
                    label={ `${ num.toString() }%` } 
                    value={ num } 
                    key={ index } 
                  />
                }
              }
              )}
            </Picker>

            <TextInput
              style={ [
                styles.input,
                this.props.riskType == '%' ? { display: 'none' } : ''
              ] }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('risk', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>SL Distance (in pips)</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('slDistance', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Levarge</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isLevargeOpen: !this.state.isLevargeOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `1:${ this.state.levarge }` }</Text>
              <Icon 
                name={ this.state.isLevargeOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.levarge }
              onValueChange={ (itemValue) => this.setState({ levarge: itemValue }) }
              style={ this.state.isLevargeOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='1:1' value={ 1 } />
              {d3.range(101).map((num, index) =>{
                if(num % 10 == 0 && num > 0) {
                  return <Picker.Item 
                    color='#3399FF' 
                    label={ `1:${ num.toString() }` } 
                    value={ num } 
                    key={ index } 
                  />
                }
              }
              )}
              <Picker.Item color='#3399FF' label='1:200' value={ 200 } />
              <Picker.Item color='#3399FF' label='1:300' value={ 300 } />
              <Picker.Item color='#3399FF' label='1:400' value={ 400 } />
              <Picker.Item color='#3399FF' label='1:500' value={ 500 } />
              <Picker.Item color='#3399FF' label='1:600' value={ 600 } />
              <Picker.Item color='#3399FF' label='1:700' value={ 700 } />
              <Picker.Item color='#3399FF' label='1:800' value={ 800 } />
              <Picker.Item color='#3399FF' label='1:900' value={ 900 } />
              <Picker.Item color='#3399FF' label='1:1000' value={ 1000 } />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Pair</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isPairOpen: !this.state.isPairOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `XXX/${ this.state.pair }` }</Text>
              <Icon 
                name={ this.state.isPairOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.pair }
              onValueChange={ (itemValue) => this.setState({ pair: itemValue }) }
              style={ this.state.isPairOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='XXX/USD' value='USD' />
              <Picker.Item color='#3399FF' label='XXX/AUD' value='AUD' />
              <Picker.Item color='#3399FF' label='XXX/GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='XXX/CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='XXX/PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='XXX/JPY' value='JPY' />
              <Picker.Item color='#3399FF' label='XXX/CAD' value='CAD' />
            </Picker>
          </View>
          <TouchableOpacity 
            style={ styles.calculateBtn }
            onPress={ () => this.calculate(
              this.state.accAmount,
              this.state.risk,
              this.state.accountCurrency,
              this.state.slDistance,
              this.state.pair,
              this.state.levarge
            ) }
          >
          <Spinner visible={ this.state.isLoadingOpen } color='#CC6600' size='large' />

            <Text style={ styles.calculateBtnTxt }>Calculate</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}

export default FxPositionSize;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  fieldsContainer: {
  },
  fieldTxt: {
    textAlign: 'center',
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  formFieldContainer: {
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#3399FF',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#353535',
    height: 40,
    paddingLeft: 10,
    textAlign: 'center'
  },
  formFieldContainerBtn: {
    flexDirection: 'row',
    backgroundColor: '#353535',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#fff',
    paddingLeft: 10
  },
  fieldContainerBtnTxt: {
    color: '#3399FF',
    fontFamily:'Avenir',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 10,
    textAlign: 'center'
  },
  accCurrencyIcon: {
    flex: 1
  },
  result: {
    flex: 0.3,
    paddingLeft: 20,
    paddingRight: 20
  },
  calculateBtn: {
    height: 38,
    backgroundColor: '#282828',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399FF'
  },
  calculateBtnTxt: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})