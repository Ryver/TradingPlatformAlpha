import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { profitCalc } from '../../CalculatorsScripts/StocksCalculators';

class StocksProfit extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.calculate = this.calculate.bind(this);

    this.state = {
      enterVal: null,
      closeVal: null,
      amount: null
    }
  }

  calculate(enterVal, closeVal, amount) {
    if(enterVal, closeVal, amount) {
      profitCalc(enterVal, closeVal, amount)
      .then((result) => {
        Alert.alert(
          'Result:',
          `Your ${ result < 0 ? 'loss' : 'profit' } is: ${ result }`
        )
      })
      .catch((err) => console.log(err))
    } else {
      Alert.alert(
        'Failed!',
        'Some required fields are empty.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>
        <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Enter Value</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('enterVal', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Close Value</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('closeVal', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Amount</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('amount', e.nativeEvent.text) }
            />
          </View>

          <TouchableOpacity 
            style={ styles.calculateBtn }
            onPress={ () => this.calculate(
              this.state.enterVal,
              this.state.closeVal,
              this.state.amount
            ) }
          >
            <Spinner visible={ this.state.isLoadingOpen } color='#CC6600' size='large' />
            <Text style={ styles.calculateBtnTxt }>Calculate</Text>
          </TouchableOpacity>
      </View>
    )
  }
}

export default StocksProfit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingLeft: 20,
    paddingRight: 20
  },
  fieldTxt: {
    textAlign: 'center',
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  formFieldContainer: {
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#3399FF',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#353535',
    height: 40,
    paddingLeft: 10,
    textAlign: 'center'
  },
  calculateBtn: {
    height: 38,
    backgroundColor: '#282828',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399FF',
    marginTop: 15
  },
  calculateBtnTxt: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})