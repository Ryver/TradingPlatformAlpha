import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import * as d3 from 'd3';
import Icon from 'react-native-vector-icons/Ionicons';
import { strikeProfitCalculator } from '../../CalculatorsScripts/BinaryOptionCalculators';

class BOStrike extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.calculate = this.calculate.bind(this);

    this.state = {
      rate: null,
      prize: 0,
      wins: null,
      isPrizeOpen: false
    }
  }

  calculate(rate, prize, wins) {
    if(rate && wins) {
      strikeProfitCalculator(rate, prize, wins)
      .then((result) => {
        Alert.alert(
          'Result:',
          `Your gross profit is: ${ result.toFixed(2) }
          Your net profit is: ${ (result - rate).toFixed(2) }`
        )
      })
      .catch((err) => console.log(err))
    } else {
      Alert.alert(
        'Failed!',
        'Some required fields are empty.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>

        <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Rate</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('rate', e.nativeEvent.text) }
            />
          </View>

        <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Prize</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isPrizeOpen: !this.state.isPrizeOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `${ this.state.prize }%` }</Text>
              <Icon 
                name={ this.state.isPrizeOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.prize }
              onValueChange={ (itemValue) => this.setState({ prize: itemValue }) }
              style={ this.state.isPrizeOpen ? '' : { display: 'none' } }
            >
              {d3.range(101).map((num, index) =>{
                return <Picker.Item 
                  color='#3399FF' 
                  label={ `${ num.toString() }%` } 
                  value={ num } 
                  key={ index } 
                />
              }
              )}
              </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Wins</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('wins', e.nativeEvent.text) }
            />
          </View>

          <TouchableOpacity 
            style={ styles.calculateBtn }
            onPress={ () => this.calculate(
              this.state.rate,
              this.state.prize,
              this.state.wins
            ) }
          >
            <Text style={ styles.calculateBtnTxt }>Calculate</Text>
          </TouchableOpacity>

      </View>
    )
  }
}

export default BOStrike;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingLeft: 20,
    paddingRight: 20
  },
  fieldTxt: {
    textAlign: 'center',
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  formFieldContainer: {
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#3399FF',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#353535',
    height: 40,
    paddingLeft: 10,
    textAlign: 'center'
  },
  formFieldContainerBtn: {
    flexDirection: 'row',
    backgroundColor: '#353535',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#fff',
    paddingLeft: 10
  },
  fieldContainerBtnTxt: {
    color: '#3399FF',
    fontFamily:'Avenir',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 10,
    textAlign: 'center'
  },
  accCurrencyIcon: {
    flex: 1
  },
  calculateBtn: {
    height: 38,
    backgroundColor: '#282828',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399FF'
  },
  calculateBtnTxt: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})