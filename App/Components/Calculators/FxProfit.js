import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { profitCalc } from '../../CalculatorsScripts/ForexCalculators';
import Spinner from 'react-native-loading-spinner-overlay';

class FxProfit extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.calculate = this.calculate.bind(this);

    this.state = {
      accCurrency: 'USD',
      pair: 'USD',
      accType: 'Standard',
      positionSize: null,
      distance: null,
      isAccCurrencyOpen: false,
      isPairOpen: false,
      isAccTypeOpen: false,
      isLoadingOpen: false
    }
  }

  calculate(posSize, pair, accCurrency, distance, accType) {
    if(posSize && distance) {
      this.setState({ isLoadingOpen: true})
      profitCalc(posSize, pair, accCurrency, distance, accType)
      .then((result) => {
        this.setState({ isLoadingOpen: false })
        Alert.alert(
          'Success!',
          `Your profit is ${ result } ${ accCurrency }`
        )
      })
      .catch((err) => console.log(err))
    } else {
      Alert.alert(
        'Failed!',
        'Some required fields are empty.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>
        <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Account Currency</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isAccCurrencyOpen: !this.state.isAccCurrencyOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ this.state.accCurrency }</Text>
              <Icon 
                name={ this.state.isAccCurrencyOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.accCurrency }
              onValueChange={ (itemValue) => this.setState({ accCurrency: itemValue }) }
              style={ this.state.isAccCurrencyOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='USD' value='USD' />
              <Picker.Item color='#3399FF' label='EUR' value='EUR' />
              <Picker.Item color='#3399FF' label='GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='JPY' value='JPY' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Pair</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isPairOpen: !this.state.isPairOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ `XXX/${ this.state.pair }` }</Text>
              <Icon 
                name={ this.state.isPairOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.pair }
              onValueChange={ (itemValue) => this.setState({ pair: itemValue }) }
              style={ this.state.isPairOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='XXX/USD' value='USD' />
              <Picker.Item color='#3399FF' label='XXX/AUD' value='AUD' />
              <Picker.Item color='#3399FF' label='XXX/GBP' value='GBP' />
              <Picker.Item color='#3399FF' label='XXX/CHF' value='CHF' />
              <Picker.Item color='#3399FF' label='XXX/PLN' value='PLN' />
              <Picker.Item color='#3399FF' label='XXX/JPY' value='JPY' />
              <Picker.Item color='#3399FF' label='XXX/CAD' value='CAD' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Account Type</Text>
            <TouchableOpacity
              onPress={ () => this.setState({ isAccTypeOpen: !this.state.isAccTypeOpen }) }
              style={ styles.formFieldContainerBtn }
            >
              <View style={{ flex: 1 }} />
              <Text style={ styles.fieldContainerBtnTxt }>{ this.state.accType }</Text>
              <Icon 
                name={ this.state.isAccTypeOpen ? 'ios-close' : 'ios-arrow-down' }
                size={ 25 }
                color='#3399FF'
                style={ styles.accCurrencyIcon }
              />
            </TouchableOpacity>
            <Picker
              selectedValue={ this.state.accType }
              onValueChange={ (itemValue) => this.setState({ accType: itemValue }) }
              style={ this.state.isAccTypeOpen ? '' : { display: 'none' } }
            >
              <Picker.Item color='#3399FF' label='Standard' value='Standard' />
              <Picker.Item color='#3399FF' label='Micro' value='Micro' />
            </Picker>
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Position Size</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('positionSize', e.nativeEvent.text) }
            />
          </View>

          <View style={ styles.formFieldContainer }>
            <Text style={ styles.fieldTxt }>Distance (in pips)</Text>
            <TextInput
              style={ styles.input }
              underlineColorAndroid='transparent'
              onChange={ (e) => this.onChange('distance', e.nativeEvent.text) }
            />
          </View>

          <TouchableOpacity 
            style={ styles.calculateBtn }
            onPress={ () => this.calculate(
              this.state.positionSize,
              this.state.pair,
              this.state.accCurrency,
              this.state.distance,
              this.state.accType
            ) }
          >
            <Spinner visible={ this.state.isLoadingOpen } color='#CC6600' size='large' />
            <Text style={ styles.calculateBtnTxt }>Calculate</Text>
          </TouchableOpacity>
      </View>
    )
  }
}

export default FxProfit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingLeft: 20,
    paddingRight: 20
  },
  fieldTxt: {
    textAlign: 'center',
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Avenir'
  },
  formFieldContainer: {
    marginBottom: 15
  },
  input: {
    fontFamily: 'Avenir',
    color: '#3399FF',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#353535',
    height: 40,
    paddingLeft: 10,
    textAlign: 'center'
  },
  formFieldContainerBtn: {
    flexDirection: 'row',
    backgroundColor: '#353535',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#fff',
    paddingLeft: 10
  },
  fieldContainerBtnTxt: {
    color: '#3399FF',
    fontFamily:'Avenir',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 10,
    textAlign: 'center'
  },
  accCurrencyIcon: {
    flex: 1
  },
  calculateBtn: {
    height: 38,
    backgroundColor: '#282828',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399FF'
  },
  calculateBtnTxt: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})