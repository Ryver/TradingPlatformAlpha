import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { 
  acceptContactReq,
  deleteContactReq
} from '../../ApiActions/contactRequests';
import { addContact } from '../../ApiActions/userRequests';
import validUserStore from '../../GlobalState/ValidUserStore';
import io from 'socket.io-client';

const socket = io.connect('http://localhost:3020');

class ContactRequest extends Component {

  constructor(props) {
    super(props);

    this.acceptContactReq = this.acceptContactReq.bind(this);
    this.cancelContactReq = this.cancelContactReq.bind(this);
  }

  cancelContactReq() {
    deleteContactReq({
      firstUsr: this.props.pending.users[0],
      secondUsr: this.props.pending.users[1]
    })
  }

  acceptContactReq() {
    acceptContactReq({
      firstUsr: this.props.pending.users[0],
      secondUsr: this.props.pending.users[1]
    })
    .then(() => {
      addContact(validUserStore.validUser.id, {
        contactUsername: this.props.pending.users[0]
      })
      .then(() => {
        socket.emit('acceptedContactReq', { 
          firstUsr: this.props.pending.users[1],
          secondUsr: this.props.pending.users[0]
         })
      })
      .catch((err) => console.log(err))
    })
    .catch((err) => console.log(err))
  }

  render() {
    
    return(
      <View style={ styles.container }>
        <View style={ styles.avatarContainer }>
          <Icon 
            name='ios-person'
            size={ 52 }
            color='#3399FF'
          />
        </View>

        <View style={ styles.usernameContainer }>
          <Text style={ styles.username }>{ this.props.pending.users[0] }</Text>
        </View>
        
        <View style={ styles.optionsContainer }>
          <TouchableOpacity 
            style={ [
              styles.requestBtn,
              { borderRightColor: '#282828' }
              ] }
            onPress={ () => this.acceptContactReq() }
          >
            <Icon 
              name='ios-checkmark'
              size={ 32 }
              color='#CC6000'
            />
          </TouchableOpacity>

          <TouchableOpacity 
            style={ styles.requestBtn }
            onPress={ () => this.cancelContactReq() }
          >
            <Icon 
              name='ios-close'
              size={ 32 }
              color='#CC6000'
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default ContactRequest;

const styles = StyleSheet.create({
  container: {
    height: 40,
    flexDirection: 'row',
    marginBottom: 20
  },
  avatarContainer: {
    flex: 2
  }, 
  usernameContainer: {
    paddingLeft: 10,
    flexDirection: 'row',
    flex: 7
  },
  username: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 22,
    marginRight: 10
  },
  optionsContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  requestBtn: {
    flex: 1,
    backgroundColor: '#282828',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#3399FF'
  }
})