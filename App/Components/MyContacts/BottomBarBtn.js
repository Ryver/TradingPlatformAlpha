import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

class BottomBarBtn extends Component {
  render() {
    return(
      <TouchableOpacity 
        style={ [
          styles.container,
          this.props.state ? { backgroundColor: '#181818' } : { backgroundColor: '#282828' }
          ] }
        onPress={ () => this.props.selectSection(this.props.fieldName) }
      >
        <Text style={ [
          styles.btnTxt,
          this.props.state ? { color: '#3399FF' } : { color: '#FFF' }
          ] }>{ this.props.btnTxt }</Text>
      </TouchableOpacity>
    )
  }
}

export default BottomBarBtn;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  btnTxt: {
    fontWeight: 'bold',
    fontFamily: 'Avenir'
  }
})