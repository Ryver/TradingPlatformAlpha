import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { deleteContactReq } from '../../ApiActions/contactRequests';
import validUserStore from '../../GlobalState/ValidUserStore';

import ContactOptions from './ContactOptions';

class Contact extends Component {

  constructor(props) {
    super(props);

    this.deleteContact = this.deleteContact.bind(this);

    this.state = {
      isOptionsListOpen: false
    }
  }

  deleteContact() {
    deleteContactReq({
      firstUsr: validUserStore.validUser.username,
      secondUsr: contact.username
    })
  }

  render() {
    return(
      <TouchableOpacity
        style={ styles.container }
        onPress={ () => this.props.nav.push({ 
          name: 'contactProfile',
          title: this.props.friend.username,
          passProps: {
            friend: this.props.friend
          }
         }) }
      >
        <View style={ styles.avatarContainer }>
          <Icon 
            name='ios-person'
            size={ 52 }
            color='#3399FF'
          />
        </View>

        <View style={ styles.usernameContainer }>
          <Text style={ styles.username }>{ this.props.friend.username }</Text>
          <IconAwesome 
            name='circle'
            size={ 12 }
            color={ this.props.friend.online ? '#ADFF2F' : '#708090' }
          />
        </View>
        
        <View style={ styles.optionsContainer }>
          <TouchableOpacity onPress={ () => this.setState({ isOptionsListOpen: !this.state.isOptionsListOpen }) }>
            <IconAwesome 
              name='ellipsis-v'
              size={ 28 }
              color='#708090'
            />
          </TouchableOpacity>
        </View>
        <ContactOptions isListOpen={ this.state.isOptionsListOpen } />
      </TouchableOpacity>
    )
  }
}

export default Contact;

const styles = StyleSheet.create({
  container: {
    height: 40,
    flexDirection: 'row',
    marginBottom: 20
  },
  avatarContainer: {
    flex: 1
  }, 
  usernameContainer: {
    paddingLeft: 10,
    flexDirection: 'row',
    flex: 8
  },
  username: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontSize: 22,
    marginRight: 10
  },
  optionsContainer: {
    flex: 1,
    alignItems: 'flex-end'
  }
})