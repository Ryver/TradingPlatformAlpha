import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import addContactStore from '../../GlobalState/AddContactStore';
import mainNavBar from '../../GlobalState/MainNavBar';
import { getPendingContactReq } from '../../ApiActions/contactRequests';

import CustomNavBar from '../CustomNavBar/CustomNavBar';
import BottomBarBtn from './BottomBarBtn';
import Contact from './Contact';
import ContactRequest from './ContactRequest';
import AddContactsModal from '../AddContacts/AddContacts';

class MyContacts extends Component {

  constructor(props) {
    super(props);

    this.selectSection = this.selectSection.bind(this);

    this.state = {
      isAllSelected: true,
      isOnlineSelected: false,
      isPendingSelected: false,
      usrPendings: []
    }
  }

  componentWillMount() {
    getPendingContactReq(this.props.user.username)
    .then((pendings) => {
      console.log(pendings)
      this.setState({ usrPendings: pendings })
    })
    .catch((err) => console.log(err))
  }

  componentWillUnmount() {
    mainNavBar.isNavBarVisible = true
  }

  selectSection(fieldName) {
    this.setState({
      isAllSelected: false,
      isOnlineSelected: false,
      isPendingSelected: false,
      [fieldName]: true
    })
  }

  render() {
    const displayFriends = this.props.user.contacts.map((contact, index) => {
      if(this.state.isAllSelected) {
        return <Contact 
          friend={ contact } 
          key={ index }
          nav={ this.props.navigator }
        />
      } else if(this.state.isOnlineSelected) {
        if(contact.online) {
         return <Contact 
          friend={ contact } 
          key={ index }
          nav={ this.props.navigator }
        />
        }
      }
    });

    const displayPendings = this.state.usrPendings.map((pending, index) => {
      if(this.state.isPendingSelected) {
        if(pending.users[1] === validUserStore.validUser.username) {
          return <ContactRequest
            pending={ pending }
            username={ pending.users[0] } 
            key={ index }
          />
        }
      }
    })

    return(
      <View style={ styles.container }>
        <CustomNavBar title='Contacts' />

        <View style={ styles.addContactBtnContainer }>
          <TouchableOpacity
            style={ styles.addContactBtn }
            onPress={ () => addContactStore.isOpen = true }
          >
            <Text style={ styles.addContactBtnTxt }>Add Contact</Text>
          </TouchableOpacity>
          <View style={{ flex: 2 }} />
        </View>

        <View style={ styles.contactsContainer }>
          <ScrollView>
            { displayFriends }
            { displayPendings }
          </ScrollView>
        </View>

        <View style={ styles.bottomBarContainer }>
          <BottomBarBtn 
            btnTxt='All'
            fieldName='isAllSelected' 
            selectSection={ this.selectSection }
            state={ this.state.isAllSelected }
          />
          <BottomBarBtn 
            btnTxt='Online' 
            fieldName='isOnlineSelected' 
            selectSection={ this.selectSection }
            state={ this.state.isOnlineSelected }
          />
          <BottomBarBtn 
            btnTxt='Pending' 
            fieldName='isPendingSelected' 
            selectSection={ this.selectSection }
            state={ this.state.isPendingSelected }
          />
        </View>
        <AddContactsModal isAddContactFormOpen={ addContactStore.isOpen }/>
      </View>
    )
  }
}

export default MyContacts;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  addContactBtnContainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1
  },
  addContactBtn: {
    flex: 1,
    backgroundColor: '#3399FF',
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  addContactBtnTxt: {
    fontFamily: 'Avenir',
    color: '#FFF'
  },
  bottomBarContainer: {
    flexDirection: 'row',
    flex: 1
  },
  contactsContainer: {
    flex: 8,
    paddingHorizontal: 20
  }
})