import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

class ContactOptions extends Component {
  render() {
    return(
      <View style={[
         styles.container,
         { display: this.props.isListOpen ? '' : 'none' }
        ] }>
        
        <TouchableOpacity onPress={ () => console.log('Delete contact.') } >
          <Text style={ styles.optionTxt }>Delete Contact</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

export default ContactOptions;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 0,
    top: 25,
    backgroundColor: '#282828',
    zIndex: 9999,
    padding: 5,
    alignItems: 'center'
  },
  optionTxt: {
    color: '#FFF',
    fontFamily: 'Avenir'
  }
})