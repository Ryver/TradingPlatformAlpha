import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import { changePassword } from '../../ApiActions/userRequests';

class ChangePasswordForm extends Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.setNewPwd = this.setNewPwd.bind(this);

    this.state = {
      existPwd: null,
      newPwd: null,
      confirmNewPwd: null
    }
  }

  setNewPwd(existPwd, newPwd, confirmNewPwd) {
    if(existPwd, newPwd, confirmNewPwd) {
      if(newPwd === confirmNewPwd) {
        changePassword(2, {
          oldPassword: existPwd,
          password: newPwd
        })
        .then((response) => {
          Alert.alert(
            response.msg === 'Password changed.' ? 'Success!' : 'Failed!',
            response.msg
          )
          if(response.msg === 'Password changed.') {
            this.setState({
              existPwd: null,
              newPwd: null,
              confirmNewPwd: null
            })
          }
        })
        .catch((err) => console.log(err))
      } else {
        Alert.alert(
          'Failed!',
          'New passwords does not match.'
        )
      }
    } else {
      Alert.alert(
        'Failed!',
        'All fields are required.'
      )
    }
  }

  onChange(fieldName, val) {
    this.setState({ [fieldName]: val })
  }

  render() {
    return(
      <View style={ styles.container }>
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='Existing password'
          secureTextEntry
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('existPwd', e.nativeEvent.text) }
          value={ this.state.existPwd }
        />
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='New password'
          secureTextEntry
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('newPwd', e.nativeEvent.text) }
          value={ this.state.newPwd }
        />
        <TextInput
          style={ [styles.input, { borderTopWidth: 0 }] }
          placeholder='Confrim New Password'
          secureTextEntry
          placeholderTextColor='#ADADAD'
          underlineColorAndroid='transparent'
          onChange={ (e) => this.onChange('confirmNewPwd', e.nativeEvent.text) }
          value={ this.state.confirmNewPwd }
        />
        <TouchableOpacity
          style={ styles.setNewPwdBtn }
          onPress = { () => this.setNewPwd(
            this.state.existPwd,
            this.state.newPwd,
            this.state.confirmNewPwd
          ) }
        >
          <Text style={ styles.btnTxt }>Set New Password</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ChangePasswordForm;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  input: {
    fontFamily: 'Avenir',
    color: '#fff',
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: '#444444',
    height: 40,
    paddingLeft: 10
  },
  setNewPwdBtn: {
    backgroundColor: '#3399FF',
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6
  },
  btnTxt: {
    color: '#fff',
    fontWeight: 'bold'
  }
})