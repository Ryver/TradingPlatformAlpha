import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

import ChangePasswordForm from './ChangePasswordForm';
import CustomNavBar from '../CustomNavBar/CustomNavBar';

class ChangePasswordScreen extends Component {
  render() {
    return(
      <View style={ styles.container }>
        <CustomNavBar title={ 'Change Password' } />
        <View style={ styles.logoContainer }>
          <Image 
            source={ require('../../Styles/Images/TradingAppLogo.png') }
            style={ styles.logo }
          />
        </View>
        <View style={ styles.formContainer }>
          <ChangePasswordForm />
        </View>
      </View>
    )
  }
}

export default ChangePasswordScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  logo: {
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
    flex: 1
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 20
  }
})