import { extendObservable } from 'mobx';

export class GlobalNavStore { 
  constructor() {
    extendObservable(this, {
      isNavOpen: false
    })
  }
}

const globalNavStore = new GlobalNavStore;

export default globalNavStore;