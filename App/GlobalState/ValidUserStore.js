import { extendObservable } from 'mobx';
import { 
  getValidUsr,
  updateContacts
 } from '../ApiActions/userRequests';
import loadingScreenState from './LoadingScreenState';
import io from 'socket.io-client';

const socket = io.connect('http://localhost:3020');

export class ValidUserStore { 
  constructor() {
    extendObservable(this, {
      isUserLogged: false,
      validUser: null
    })
  }

  async setValidUser(token) {
    getValidUsr(token)
    .then((user) => {
      if(user) {
        this.isUserLogged = true
        this.validUser = user
      }
    })
    .then(() => {
      loadingScreenState.isLoading = false
    })
    .catch((err) => console.log(err))
  };

  updateContacts() {
    socket.on(`newContactAccepted/${ this.validUser.username }`, (data) => {
      updateContacts(this.validUser.id)
      .then((response) => {
        this.validUser.contacts = response.updateContacts
      })
    })
  }
}

const validUserStore = new ValidUserStore;

export default validUserStore;