import { extendObservable } from 'mobx';

export class LoadingScreenState {
  constructor() {
    extendObservable(this, {
      isLoading: true
    })
  }
}

const loadingScreenState = new LoadingScreenState;

export default loadingScreenState;