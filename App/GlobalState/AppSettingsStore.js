import { extendObservable } from 'mobx';
import { AsyncStorage } from 'react-native'

export class AppSettingsStore {
  constructor() {
    extendObservable(this, {
      globalSounds: false,
      notificationsDisplay: true,
      notificationsVibrate: true,
      notificationsSounds: true,
      diariesAutoSave: true,
      diariesSavePosSize: true,
      diariesPairsName: true,
      sharingDiary: true
    })
  }

  settingsObj() {
    AsyncStorage.getItem('TradingAppSettings')
    .then((result) => {
      if(result) {
        const res = JSON.parse(result);

        this.globalSounds = res.globalSounds,
        this.notificationsDisplay = res.notificationsDisplay,
        this.notificationsVibrate = res.notificationsVibrate
        this.notificationsSounds = res.notificationsSounds,
        this.diariesAutoSave = res.diariesAutoSave,
        this.diariesSavePosSize = res.diariesSavePosSize,
        this.diariesPairsName = res.diariesPairsName,
        this.sharingDiary = res.sharingDiary
      } else {
        AsyncStorage.setItem('TradingAppSettings', JSON.stringify({
          globalSounds: true,
          notificationsDisplay: true,
          notificationsVibrate: true,
          notificationsSounds: true,
          diariesAutoSave: true,
          diariesSavePosSize: true,
          diariesPairsName: true,
          sharingDiary: true
        }));
      }
    })
  }
}

const appSettingsStore = new AppSettingsStore;

export default appSettingsStore;