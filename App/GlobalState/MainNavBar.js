import { extendObservable } from 'mobx';

export class MainNavBar { 
  constructor() {
    extendObservable(this, {
      isNavBarVisible: true
    })
  }
}

const mainNavBar = new MainNavBar

export default mainNavBar;