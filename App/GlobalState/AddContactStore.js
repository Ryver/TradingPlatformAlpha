import { extendObservable } from 'mobx';

export class AddContactStore { 
  constructor() {
    extendObservable(this, {
      isOpen: false
    })
  }
}

const addContactStore = new AddContactStore;

export default addContactStore;