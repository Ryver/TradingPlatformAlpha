import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  AsyncStorage,
  AppState
} from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import Icon from 'react-native-vector-icons/Ionicons';
import mainNavBar from './GlobalState/MainNavBar';
import { observer } from 'mobx-react';
let nav;
import appSettingsStore from './GlobalState/AppSettingsStore';
import validUserStore from './GlobalState/ValidUserStore';
import globalNavStore from './GlobalState/GlobalNavStore';
import loadingScreenState from './GlobalState/LoadingScreenState';
import { changeOnlineStatus } from './ApiActions/userRequests';
import io from 'socket.io-client';
import PushNotification from 'react-native-push-notification';

const socket = io.connect('http://localhost:3020');

import SignInScreen from './Components/SignInScreen/SignInScreen';
import SignUpScreen from './Components/SignUpScreen/SignUpScreen';
import ForgotPasswordScreen from './Components/ForgotPasswordScreen/ForgotPasswordScreen';
import Navigation from './Components/Navigation/Navigation';
import CalculatorsListView from './Components/CalculatorsListView/CalculatorsListView';
import FxPositionSize from './Components/Calculators/FxPositionSize';
import SettingsView from './Components/SettingsView/SettingsView';
import ChangePasswordScreen from './Components/ChangePasswordScreen/ChangePasswordScreen';
import MyProfile from './Components/MyProfile/MyProfile';
import Notes from './Components/Notes/Notes';
import FxProfit from './Components/Calculators/FxProfit';
import BOStrike from './Components/Calculators/BOStrike';
import LoadingScreen from './Components/LoadingScreen/LoadingScreen';
import FxSL from './Components/Calculators/FxSL';
import StocksProfit from './Components/Calculators/StocksProfit';
import MyContacts from './Components/MyContacts/MyContacts';
import ContactProfile from './Components/ContactProfile/ContactProfile';
import ChartsHolder from './Components/Charts/ChartsHolder';
import Note from './Components/Note/Note';
import PushNotificationController from './Components/PushNotificationController/PushNotificationController';
import AddNewPost from './Components/AddNewPost/AddNewPost';

class App extends Component {

  constructor(props) {
    super(props);

    this.renderScene = this.renderScene.bind(this);
    this.setNavBar = this.setNavBar.bind(this);
    this.handleAppState = this.handleAppState.bind(this);
  }

  handleAppState(currentState) {
    if(currentState === 'background') {
      if(validUserStore.validUser) {
        changeOnlineStatus(validUserStore.validUser.id, { state: false })
        .then(() => {
          socket.emit('changeOnlineStatus', 'online status')
        })
        .catch((err) => console.log(err))
      }
    } else if(currentState === 'active') {
      if(validUserStore.validUser) {
        changeOnlineStatus(validUserStore.validUser.id, { state: true })
        .then(() => {
          socket.emit('changeOnlineStatus', 'online status')
        })
        .catch((err) => console.log(err))
      }
    }
  }

  componentWillMount() {
    appSettingsStore.settingsObj();
    
    AsyncStorage.getItem('TradingAppUsrToken')
    .then((token) => {
      if(token) {
        validUserStore.setValidUser(token)
      } else {
        loadingScreenState.isLoading = false
      }
    })
    .catch((err) => console.log(err))

    if(validUserStore.validUser) {
      socket.on(`contactInvite${ validUserStore.validUser.username }`, (data) => {
        PushNotification.localNotification({
          message: `You have one new contact request from ${ data.user }.`,
          number: 2,
          playSound: appSettingsStore.notificationsSounds,
          vibrate: appSettingsStore.notificationsVibrate,
          vibration: 300
        })
      })
    }
  }
  
  componentDidMount() {
    AppState.addEventListener('change', this.handleAppState);
  }

  componentWillUnmount() {
    AppState.addEventListener('change', this.handleAppState);
  }

  setNavBar(NavigationRouteMapper) {
    return(
      mainNavBar.isNavBarVisible ? 
      <Navigator.NavigationBar 
        routeMapper={ NavigationRouteMapper }
      /> : null
    )
  }

  renderScene(route, navigator) {
    nav = navigator
    switch(route.name) {
      case 'signInScreen':
      return <SignInScreen navigator={ navigator } />
      case 'signUpScreen':
      return <SignUpScreen navigator={ navigator } />
      case 'forgotPasswordScreen':
      return <ForgotPasswordScreen navigator={ navigator } />
      case 'calculators':
      return <CalculatorsListView 
        navigator={ navigator }
        { ...route.passProps } 
      />
      case 'fxPositionSize':
      return <FxPositionSize
        navigator={ navigator }
        { ...route.passProps } 
      />
      case 'settingsScreen':
      return <SettingsView navigator={ navigator } />
      case 'changePasswordScreen':
      return <ChangePasswordScreen navigator={ navigator } />
      case 'myProfile':
      return <MyProfile 
        navigator={ navigator } 
        user={ validUserStore.validUser ? validUserStore.validUser : null }
      />
      case 'notes':
      return <Notes 
        navigator={ navigator } 
        { ...route.passProps }
      />
      case 'fxProfit':
      return <FxProfit />
      case 'boStrike':
      return <BOStrike />
      case 'fxStopLoss':
      return <FxSL />
      case 'stocksProfit':
      return <StocksProfit />
      case 'myContacts':
      return <MyContacts 
        navigator={ navigator } 
        user={ validUserStore.validUser ? validUserStore.validUser : null }
      />
      case 'contactProfile':
      return <ContactProfile { ...route.passProps }/>
      case 'charts':
      return <ChartsHolder />
      case 'note':
      return <Note 
        navigator={ navigator } 
        { ...route.passProps }
      />
      case 'newPost':
      return <AddNewPost 
        navigator={ navigator } 
        { ...route.passProps }
      />
    }
  }

  render() {
    const NavigationRouteMapper = {
      LeftButton(route, navigator,index, navState) {
        if(index > 0) {
          return(
            <TouchableOpacity
              style={ styles.navBarBtn }
              onPress={ () => {
                mainNavBar.isNavBarVisible = false
                 navigator.pop() 
              } }
            >
              <Icon 
                name='ios-arrow-back'
                size={ 25 }
                color='#CC6600'
              />
              <Text style={ styles.navBarBtnTxt }>Back</Text>
            </TouchableOpacity>
          )
        } else {
          return null;
        }
      },
      RightButton(route, navigator, index, navState) {
        return null;
      },
      Title(route, navigator, index, navState) {
        if(index > 0) {
         return <Text style={ styles.navBarTitle }>{ route.title }</Text>
        } else {
          return null;
        }
      }
    }

    if(loadingScreenState.isLoading) {
      return <LoadingScreen />
    } else {
      return(
        <View style={ styles.container }>
          <PushNotificationController />
          <StatusBar 
            barStyle='light-content'
          />
          <Navigator
            initialRoute={{ name: validUserStore.isUserLogged ? 'myProfile' : 'signInScreen' }}
            renderScene={ this.renderScene }
            navigationBar={ this.setNavBar(NavigationRouteMapper) }
            style={ styles.navScreen }
          />
          <Navigation 
            navigator={ nav } 
            isMenuOpen={ globalNavStore.isNavOpen }
          />
        </View>
      )
    }
  }
}

export default observer(App);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navScreen: {
    flex: 1
  },
  navBarBtn: {
    flexDirection: 'row',
    paddingLeft: 10
  },
  navBarBtnTxt: {
    marginLeft: 10,
    fontSize: 16,
    color: '#CC6600',
    fontFamily: 'Avenir'
  },
  navBarTitle: {
    color: '#FFF',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 16
  }
});